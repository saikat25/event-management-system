package EventPakageCreate;

import java.awt.Color;
import java.awt.Dialog.ModalityType;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.util.Date;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.sun.xml.internal.messaging.saaj.soap.JpegDataContentHandler;
import com.toedter.calendar.JDateChooser;

import Login_win.BackGround;
import Login_win.LoginWindow;
import SelectedEvent.selectedEvent;

public class customizePakageDate extends JDialog implements ActionListener{
	
	//JPanel backGroundPanel = new JPanel();
    JDateChooser dateChooser;  
    JButton next;
    selectedEvent packageDatePass;

	
	public customizePakageDate(selectedEvent packageDate)
	{
		
		super(packageDate, ModalityType.APPLICATION_MODAL);
		setTitle("Select Category & Item");
		setSize(600, 500);
		//packageDate.hideWindow();
		packageDate.dispose();
		packageDatePass = packageDate;
		  //***************** JPanel option *****************
		
		 ImageIcon backGroundImage = new ImageIcon(LoginWindow.class.getResource("/background/8.jpg"));
			BackGround backGroundPanel = new BackGround(backGroundImage); // adding background
		    backGroundPanel.setLayout(null);
	        add(backGroundPanel);

	
	        
			//************************* Date chooser***********************
			  
			
	        try {
	        	
	              dateChooser = new JDateChooser();
				  dateChooser.setBounds(200, 120, 175, 25);
				  backGroundPanel.add( dateChooser);
//				  Date d=new Date();
//				 Date date= dateChooser.getDate();
//				 String d1 = date.toString();
				 
	           //   String strDate = DateFormat.getDateInstance().format(date); // Change date into string formate 
				 //  String dateFromDateChooser = dateChooser.getDate().toString();
				  //  lblDate.setText(dateFromDateChooser);
				  //  lblDateFormat.setText(dateChooser.getDateFormatString().toString());
				  //  System.out.println(dateFromDateChooser);
				// System.out.println(dateFromDateChooser);
				
			} catch (Exception e) {
				System.err.println(e.getMessage());
			}
	   
	        
	        //*******************************JCalender*****************
	        
//	        JDateChooser dateField;
//	        Date d=new Date();
//	        dateField= new JDateChooser(d);
//	        dateField.setBounds(470,70,110,25);

	    //    how to get date from datefield
//	        Date date=dateField.getDate();
				
				  // ************** JLabel (Select Date) *******************
				  
				  JLabel categorTitle = new JLabel("Select booking date");   //Createcustomize Pakage category title Label
				  categorTitle.setBounds(200, 80, 200, 25);
				  categorTitle.setFont(new Font("Serif",Font.BOLD,20));
				  backGroundPanel.add( categorTitle);				
				
			  
		// *************************** JButton (Next)************
			
				  Action nextAction = new AbstractAction("Next") {

						public void actionPerformed(ActionEvent arg0) {
						


						}
					};
				 
				 next= new JButton("Next");
				 next.setBounds(205, 330, 70, 25);
				 next.setFont(new Font("Serif",Font.BOLD,15));
				 next.addActionListener(this);
				  backGroundPanel.add(next);
				  
				  
					// *************************** JButton (Go back)************
					
				  Action goBackAction = new AbstractAction("Go back") {

						public void actionPerformed(ActionEvent arg0) {
						
							customizePakageDate.this.dispose();
							packageDate.showWindow();
						}
					};
				 
				 JButton goBack= new JButton(goBackAction);
				 goBack.setBounds(420, 330, 90, 25);
				 goBack.setFont(new Font("Serif",Font.BOLD,15));
				  backGroundPanel.add(goBack);					  
				  
		 // *********** JFrame option ************
	    setUndecorated(false);  
	    setLocationRelativeTo(null);
	    setResizable(false);
	    setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);	
	    setVisible(true);
		
		
	}
	
	


	public void hideWindow() {                  // hide window 
		this.setVisible(false);
	}
	
	public void showWindow() {					// show window
		this.setVisible(true);
	}
	
	public String packageDate()
	{	
		Date date= dateChooser.getDate();
		String strDate = DateFormat.getDateInstance().format(date);
		return strDate;
	}
	
	

	public static void main(String[] args) {
		
	//	new customizePakageDate();
	}




	@Override
	public void actionPerformed(ActionEvent e) {

		if(e.getSource() == next)
		{
			
			
		//*********************One way**********************	
//			Date date= dateChooser.getDate();
//			String strDate = DateFormat.getDateInstance().format(date);
			packageDate();
		    //System.out.println(strDate);
			 
		//*************************2nd way**************************	 
//			     Date d=new Date();
//				 Date date= dateChooser.getDate();
//				 String d1 = date.toString();
//				 System.out.println(d1);
			
//********************************3rd way********************
			//   String dateFromDateChooser = dateChooser.getDate().toString();
//				
			//   System.out.println(dateFromDateChooser);
			 
//******************************4th way (Show only the formate)*************************
//			 
//			 String dateFromDateChooser = dateChooser.getDateFormatString().toString();
//				    System.out.println(dateFromDateChooser);							 
			 
			new customizePakageCategoryItemSelect(customizePakageDate.this,packageDatePass);
			customizePakageDate.this.dispose();
			
		}
			
		
	}

}
