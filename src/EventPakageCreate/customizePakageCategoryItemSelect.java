package EventPakageCreate;

import java.awt.Color;
import java.awt.Dialog.ModalityType;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import Login_win.BackGround;
import Login_win.LoginWindow;
import Login_win.Login_window_db;
import SelectedEvent.selectedEvent;
import pakageBookingForm.customizePakageBooking;



public class customizePakageCategoryItemSelect extends JDialog implements ActionListener {
	
	//JPanel backGroundPanel = new JPanel();
	
	 selectedEvent packageSelect;
	 String eventName,unEditableTC,selectedCategoryTitle;
	 Vector<String> cTL = new Vector<String>();
	 Vector<String> uneditableList = new Vector<String>();
	 Vector<String> cItemList = new Vector<String>();
	 Login_window_db callDB = new Login_window_db();
	  JTextField  totalChargeField;
	  JButton next,addToPackageList;
	  selectedEvent  packageDateToMove;
	  customizePakageDate itemSelectPass;
	  
	  JComboBox<String >  categoryTitleList , categoryItemList;
	  
	  
	public customizePakageCategoryItemSelect(customizePakageDate itemSelect, selectedEvent  packageDate)
	{
		
		super (itemSelect, ModalityType.APPLICATION_MODAL);
		setTitle("Select Category & Item");
		setSize(600, 500);
		itemSelect.hideWindow();
		packageDateToMove = packageDate;
		itemSelectPass = itemSelect;
		
		  //***************** JPanel option *****************
		
		 ImageIcon backGroundImage = new ImageIcon(LoginWindow.class.getResource("/background/8.jpg"));
			BackGround backGroundPanel = new BackGround(backGroundImage); // adding background
		    backGroundPanel.setLayout(null);
	        add(backGroundPanel);
		
		
				//createFixedPakagePanel.setLayout(null);
				//createFixedPakagePanel.setBackground(Color.CYAN);
				//add(createFixedPakagePanel);
				
				
				
				  // ************** JLabel (Category Title) *******************
				  
				  JLabel categorTitle = new JLabel("Category title :");   //Createcustomize Pakage category title Label
				  categorTitle.setBounds(80, 80, 140, 25);
				  categorTitle.setFont(new Font("Serif",Font.BOLD,15));
				  backGroundPanel.add( categorTitle);
				  
				  eventName =packageDate.eventLabel(); 
				  cTL = callDB.categoryTitleForSelectedEvent(eventName);
				  
				  categoryTitleList = new JComboBox<String>(cTL);
				  categoryTitleList.setBounds(200, 80, 160, 25);
				  backGroundPanel.add( categoryTitleList);
				  categoryTitleList.setSelectedItem(null);
				  categoryTitleList.addActionListener(this);
				  
				  
	  // ************** JLabel (Category Item) *******************
				  
				  JLabel categoryItem = new JLabel("Category item :");   //Createcustomize Pakage category Item Label
				  categoryItem.setBounds(80, 150, 140, 25);
				  categoryItem.setFont(new Font("Serif",Font.BOLD,15));
				  backGroundPanel.add( categoryItem);
				  
				  
				  categoryItemList = new JComboBox<String>();
				  categoryItemList.setBounds(200, 150, 160, 25);
				  backGroundPanel.add( categoryItemList);
				  categoryItemList.setSelectedItem(null);
		
				  
				  
	  // ************** JLabel (Total Charge) *******************
				  
				  JLabel totalCharge = new JLabel("Total charge :");   //Createcustomize Pakage category title Label
				  totalCharge.setBounds(80, 220, 140, 25);
				  totalCharge.setFont(new Font("Serif",Font.BOLD,15));
				  backGroundPanel.add( totalCharge);
				  
				  
				  totalChargeField = new JTextField();
				  totalChargeField.setBounds(200, 220, 160, 25);
				  backGroundPanel.add( totalChargeField);
				  
				  
		// *************************** JButton (Next)************
			
//				  Action nextAction = new AbstractAction("Next") {
//
//						public void actionPerformed(ActionEvent arg0) {
//						
//							new customizePakageBooking(customizePakageCategoryItemSelect.this , packageDate);												
//						}
//					};
				 
				 next= new JButton("Next");
				 next.setBounds(430, 300, 70, 25);
				 next.setFont(new Font("Serif",Font.PLAIN,15));
				 next.addActionListener(this);
				 backGroundPanel.add(next);
				  
		//*************************Add to package List**************
				 
				 addToPackageList= new JButton("Add to Package List");
				 addToPackageList.setBounds(200, 300, 180, 25);
				 addToPackageList.setFont(new Font("Serif",Font.PLAIN,15));
				 addToPackageList.addActionListener(this);
				 backGroundPanel.add(addToPackageList);
				 
				 
				 
		 // *********** JFrame option ************
	    setUndecorated(false);  
	    setLocationRelativeTo(null);
	    setResizable(false);
	   // setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);	
	    setVisible(true);
		
		
	}
	
	
	
	
	public void hideWindow() {                  // hide window 
		this.setVisible(false);
	}
	
	public void showWindow() {					// show window
		this.setVisible(true);
		
	}
	
	public Vector<String> getSelectedEvent()
	{
		return cItemList ;
	}
	
	
	public String uneditableInfoForBookingForm () {
		
		unEditableTC = (totalChargeField.getText());
		return unEditableTC;
		
	}

	public static void main(String[] args) {
	
//	new customizePakageCategoryItemSelect(itemSelect, packageDate)

	}




	@Override
	public void actionPerformed(ActionEvent e) {
		
		Vector<String> packageInfoList = new Vector<String>();
		

		
		if(e.getSource() == addToPackageList)
		{
			
			packageInfoList.add(eventName);
			packageInfoList.add(categoryItemList.getSelectedItem().toString());
			packageInfoList.add(totalChargeField.getText());
			callDB.createCustomizePackage(packageInfoList);
			
			totalChargeField.setText(null);
		
		
		}
		

		else if (e.getSource() == categoryTitleList)
		{
               if( categoryTitleList.getSelectedItem().toString() != null )
				
			 {
            	   categoryItemList.setSelectedItem(null);
            	   categoryItemList.removeAllItems();
				
			 }
      
            	   selectedCategoryTitle = categoryTitleList.getSelectedItem().toString();  
            	   
            	   System.out.println(eventName);
            	   
            	   System.out.println(selectedCategoryTitle);
            	   
            	   cItemList= callDB.categoryItemForSelectedCT(eventName,selectedCategoryTitle);
            	   
       			int i ;
       			
       			categoryItemList.addItem(null);
       			
    			for(i=0;i<cItemList.size();i++)
    				
    			{
    				//categoryTitleList.setSelectedItem(null);
    				categoryItemList.addItem(cItemList.elementAt(i));
    				System.out.println(cItemList.elementAt(i));
    				
    			}
	

		}
		
		else if (e.getSource() == next)
		{
			
			new customizePakageBooking(customizePakageCategoryItemSelect.this , packageDateToMove,itemSelectPass);
			
		}
		
		
		
	}

}
