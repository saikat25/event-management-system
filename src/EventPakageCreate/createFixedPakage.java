package EventPakageCreate;

import java.awt.Color;
import java.awt.Dialog.ModalityType;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

import javafx.scene.control.ComboBox;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import Login_win.BackGround;
import Login_win.LoginWindow;
import Login_win.Login_window_db;
import PostLogin_win.AfterLogin;
import SelectedEvent.selectedEvent;

public class createFixedPakage extends JDialog implements ActionListener {
	
	//JPanel backGroundPanel = new JPanel();
	
	String eventName;
	Login_window_db callDB = new Login_window_db();
	Vector<String> cTL = new Vector<String>();
	Vector<String> packageList = new Vector<String>();
	JButton done;
	JTextField priceField,pakageNameField;
	JComboBox<String > categoryList;
	AfterLogin sd ;
	
	public createFixedPakage(selectedEvent fixedPackage,AfterLogin passName)
	
	{
		
		super(fixedPackage,ModalityType.APPLICATION_MODAL);
		setTitle("Create Fixed Pakage");
		setSize(600, 500);
		fixedPackage.dispose();
	
		  //***************** JPanel option *****************
		
		 ImageIcon backGroundImage = new ImageIcon(LoginWindow.class.getResource("/background/8.jpg"));
			BackGround backGroundPanel = new BackGround(backGroundImage); // adding background
		    backGroundPanel.setLayout(null);
	        add(backGroundPanel);
		
		
	//	createFixedPakagePanel.setLayout(null);
	//	createFixedPakagePanel.setBackground(Color.CYAN);
	//	add(createFixedPakagePanel);
		
		
		//*********************** JLabel (Pakage Name)**************
		
		JLabel pakageName = new JLabel("Pakage name :");   //Create Fixed Pakage Label
		pakageName.setBounds(80, 80, 110, 25);
		pakageName.setFont(new Font("Serif",Font.BOLD,15));
		backGroundPanel.add(pakageName);
		
		
		  pakageNameField = new JTextField(); // pakage name feild
		  pakageNameField.setBounds(200, 82, 150, 25);
		  pakageNameField.setToolTipText("Enter the PakageName");
		  backGroundPanel.add(pakageNameField);
		
		  // ************** JLabel (Category Title) *******************
		  
		  
		  
		  JLabel categoryName = new JLabel("Category title :");   //Create Fixed Pakage category title Label
		  categoryName.setBounds(80, 150, 110, 25);
		  categoryName.setFont(new Font("Serif",Font.BOLD,15));
		  backGroundPanel.add(categoryName);
		  
		  
		  eventName =fixedPackage. eventLabel ();
		  cTL = callDB.categoryTitleForSelectedEvent(eventName);
		  
		  categoryList = new JComboBox<String>(cTL);
		  categoryList.setBounds(200, 150, 149, 25);
		  backGroundPanel.add(categoryList);
		  categoryList.setSelectedItem(null);
		  
		  
		//*********************** JLabel (Price)**************
			
			JLabel price = new JLabel("Total charge :");   // Price Label
			price.setBounds(80, 220, 110, 25);
			price.setFont(new Font("Serif",Font.BOLD,15));
			backGroundPanel.add(price);
			
			
			   priceField = new JTextField(); // Price feild
			  priceField.setBounds(200, 220, 150, 25);
			  priceField.setToolTipText("Enter the PakageName");
			  backGroundPanel.add(priceField);
			  
			  
				 //*********************** JButton (Done Button)*************
				 
//				 Action doneAction = new AbstractAction("Done") {
//
//						public void actionPerformed(ActionEvent arg0) {
//						
//																			
//						}
//					};
//				 
				 done= new JButton("Done");
				 done.setBounds(200, 370, 80, 25);
				 done.addActionListener(this);
				 backGroundPanel.add(  done);
				 
 //*********************** JButton (Go done Button)*************
				 
				 Action goBackAction = new AbstractAction("Go back") {

						public void actionPerformed(ActionEvent arg0) {
						
							createFixedPakage.this.dispose();
							passName.showWindow();

							selectedEvent SE = new selectedEvent(passName);
							SE.pakageList =  new JComboBox(callDB.FixedPackageList(eventName));
							
						}
					};
				 
				 JButton goBack= new JButton(goBackAction);
				 goBack.setBounds(450, 370, 80, 25);
				 backGroundPanel.add(goBack);
		
		
		 // *********** JFrame option ************
	    setUndecorated(false);  
	    setLocationRelativeTo(null);
	    setResizable(false);
	    setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);	
	    setVisible(true);
		
		
		
	}

	public static void main(String[] args) {
		
		//new createFixedPakage();
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		
		Vector<String> packageInfoList = new Vector<String>();
		String ct =categoryList.getSelectedItem().toString() ;
		
		packageInfoList.add(pakageNameField.getText());
		packageInfoList.add(ct);
		packageInfoList.add(" ");
		packageInfoList.add(eventName);
		packageInfoList.add(priceField.getText());
		
		if(arg0.getSource() == done)
		{

				callDB.createFiexedPakage(packageInfoList);
				pakageNameField.setText(null);
				priceField.setText(null);
	
		}
		
		
		
	}

}
