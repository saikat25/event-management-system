package EventPakageCreate;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import Login_win.BackGround;
import Login_win.LoginWindow;
import SelectedEvent.selectedEvent;

public class categoryItem_FixedPakage extends JFrame {
	
	//JPanel backGroundPanel = new JPanel();
	
	public categoryItem_FixedPakage()
	
	{
		super ("Create Fixed Pakage");
		setSize(400, 300);
		
		  //***************** JPanel option *****************
		
		 ImageIcon backGroundImage = new ImageIcon(LoginWindow.class.getResource("/background/8.jpg"));
			BackGround backGroundPanel = new BackGround(backGroundImage); // adding background
		    backGroundPanel.setLayout(null);
	        add(backGroundPanel);
		
		//createFixedPakagePanel.setLayout(null);
		//createFixedPakagePanel.setBackground(Color.CYAN);
		//add(createFixedPakagePanel);
		
		
		  // ************** JLabel (Category Title) *******************
		  
		  JLabel categoryItem = new JLabel("Select category item");   //Create Fixed Pakage category title Label
		  categoryItem.setBounds(100, 30, 200, 25);
		  categoryItem.setFont(new Font("Serif",Font.BOLD,20));
		  backGroundPanel.add( categoryItem);
		  
		  
		  JComboBox<String >  categoryItemList = new JComboBox<String>();
		  categoryItemList.setBounds(120, 80, 160, 25);
		  backGroundPanel.add( categoryItemList);
		  
		  
			 //*********************** JButton (OK Button)*************
			 
			 Action okAction = new AbstractAction("OK") {

					public void actionPerformed(ActionEvent arg0) {
					
					//	new createFixedPakage();												
					}
				};
			 
			 JButton ok= new JButton(okAction);
			 ok.setBounds(170, 210, 60, 25);
			 backGroundPanel.add(ok);
		  

		 // *********** JFrame option ************
	    setUndecorated(false);  
	    setLocationRelativeTo(null);
	    setResizable(false);
	    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);	
	    setVisible(true);
		
		
		
	}

	public static void main(String[] args) {
		
		new categoryItem_FixedPakage();

	}

}
