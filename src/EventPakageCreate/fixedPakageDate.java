package EventPakageCreate;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.util.Date;
import java.util.Properties;
import java.util.Vector;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFormattedTextField.AbstractFormatter;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.toedter.calendar.JDateChooser;

import Login_win.BackGround;
import Login_win.LoginWindow;
import Login_win.Login_window_db;
import PostLogin_win.AfterLogin;
import SelectedEvent.fixedPakageDetail;
import SelectedEvent.selectedEvent;
import pakageBookingForm.fixedPakageBooking;

public class fixedPakageDate extends JDialog implements ActionListener {
	
//JPanel  backGroundPanel = new JPanel();
	 JButton pakageDetail,next;
	 selectedEvent packageInfo;
	 Login_window_db callDB;
	 selectedEvent packageInfo_Date;
	 String evName,pacName;
	 JDateChooser dateChooser;
	 selectedEvent packageDatePass;

	
	
	public fixedPakageDate(selectedEvent packageDate)
	{
		
		super(packageDate , ModalityType.APPLICATION_MODAL);
		setTitle("Select Date");
		setSize(600, 500);
		packageDate.hideWindow();
		packageDatePass = packageDate;
		  //***************** JPanel option *****************
		
		 ImageIcon backGroundImage = new ImageIcon(LoginWindow.class.getResource("/background/8.jpg"));
			BackGround backGroundPanel = new BackGround(backGroundImage); // adding background
		    backGroundPanel.setLayout(null);
	        add(backGroundPanel);
		
		
			//	 fixedPakageDatePanel.setLayout(null);
			//	 fixedPakageDatePanel.setBackground(Color.CYAN);
			//	add( fixedPakageDatePanel);
	        
	        evName= packageDate.eventLabel();
	        pacName = packageDate.selectEventPakage();
	        

				  // ************** JLabel (Select Date) *******************
				  
				  JLabel date = new JLabel("Select booking date");   //Fixed Pakage Date Label
				  date.setBounds(200, 80, 200, 26);
				  date.setFont(new Font("Serif",Font.BOLD,21));
				  backGroundPanel.add( date);	
				   
				   
		//************************* Date chooser***********************
				  
				  dateChooser = new JDateChooser();
				  dateChooser.setBounds(200, 120, 175, 25);
				  backGroundPanel.add( dateChooser);
				
			  
		// *************************** JButton (Next)************
			
//				  Action nextAction = new AbstractAction("Next") {
//
//						public void actionPerformed(ActionEvent arg0) {
//						
//							new fixedPakageBooking(fixedPakageDate.this, packageDate);												
//						}
//					};
				 
				 next= new JButton("Next");
				 next.setBounds(205, 330, 70, 25);
				 next.setFont(new Font("Serif",Font.BOLD,15));
				 next.addActionListener(this);
				 backGroundPanel.add(next);
				  
				  
					// *************************** JButton (Package detail)************
					
				  Action pakageDetailAction = new AbstractAction("Package detail") {

						public void actionPerformed(ActionEvent arg0) {
						
							 //new fixedPakageDetail(fixedPakageDate.this);
//							 Vector<String> packageinformation = new Vector<String>();
//							 Vector<String> infoFromDB = new Vector<String>();
//							 
//							 packageinformation.add(evName);
//							 packageinformation.add(pacName);
						      
						     // System.out.println(information.elementAt(0));
							 //    System.out.println(information.elementAt(1));   
						      				
							new fixedPakageDetail(fixedPakageDate.this);
							//callDB.fixedPackageDetail();
								
						}
					};
				 
				 pakageDetail= new JButton(pakageDetailAction);
				 pakageDetail.setBounds(420, 82, 130, 25);
				 pakageDetail.setFont(new Font("Serif",Font.BOLD,15));
				 backGroundPanel.add(pakageDetail);	
				  
				  
				// *************************** JButton (Go back)************
					
				  Action goBackAction = new AbstractAction("Go back") {

						public void actionPerformed(ActionEvent arg0) {
						
							fixedPakageDate.this.dispose();
							packageDate.showWindow();
						}
					};
				 
				 JButton goBack= new JButton(goBackAction);
				 goBack.setBounds(420, 330, 90, 25);
				 goBack.setFont(new Font("Serif",Font.BOLD,15));
				   backGroundPanel.add(goBack);	
				   
	  
		 // *********** JFrame option ************
	    setUndecorated(false);  
	    setLocationRelativeTo(null);
	    setResizable(false);
	    setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);	
	    setVisible(true);

	}
	
	
	
	public void hideWindow() {                  // hide window 
		this.setVisible(false);
	}
	
	public void showWindow() {					// show window
		this.setVisible(true);
	}
	
	public String packageDate()
	{	
		Date date= dateChooser.getDate();
		String strDate = DateFormat.getDateInstance().format(date);
		return strDate;
	}
	
	public static void main(String[] args) {
	//	new fixedPakageDate();

	}



	@Override
	public void actionPerformed(ActionEvent e) {
		
		if(e.getSource() == next)
		{
			packageDate();
			new fixedPakageBooking(fixedPakageDate.this, packageDatePass);
			
			
		}
		
	
//		if(e.getSource() == pakageDetail)
//		{
//			
//			 Vector<String> packageinformation = new Vector<String>();
//			 Vector<String> infoFromDB = new Vector<String>();
//			 
//			 packageinformation.add(evName);
//			 packageinformation.add(pacName);
//		      
//		     // System.out.println(information.elementAt(0));
//			 //    System.out.println(information.elementAt(1));   
//		      
//			callDB.fixedPackageDetail(packageinformation);
//			new fixedPakageDetail(fixedPakageDate.this);
//				
//		   
//				
//			
//			
//		}
		
		
		
	}

}
