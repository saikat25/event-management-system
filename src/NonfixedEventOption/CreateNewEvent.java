package NonfixedEventOption;

import java.awt.Font;
import java.awt.Dialog.ModalityType;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JTextField;

import Login_win.BackGround;
import Login_win.LoginWindow;
import Login_win.Login_window_db;
import NonfixedEvent.NonfixedSelectedEvent;
import SelectedEvent.SelectEventType;

public class CreateNewEvent extends JDialog implements ActionListener{

	JLabel eventName,categoryName;
	JTextField eventField,categoryField;
	JButton addCategoryTitle, addSubCategory ,goback ;
	NonfixedSelectedEvent createEventback;
	Login_window_db callDB  = new Login_window_db();;
	
	
	
	
	

	public CreateNewEvent(NonfixedSelectedEvent createEvent)
	{
		
		
		super(createEvent,ModalityType.MODELESS) ;
		setTitle("Create New Event");
		setSize(450, 350);
		createEvent.hideWindow();
		
		
	    
	    //***************** JPanel option *****************
		
		    ImageIcon backGroundImage = new ImageIcon(LoginWindow.class.getResource("/background/8.jpg"));
			BackGround backGroundPanel = new BackGround(backGroundImage); // adding background
		    backGroundPanel.setLayout(null);
	        add(backGroundPanel);
	        
	        
	    	//************ JLable option **************
			
			JLabel eventName = new JLabel("Event name :");   // Event Name Label
			eventName.setBounds(30, 50, 110, 27);
			eventName.setFont(new Font("Serif",Font.BOLD,15));
		  
			
			backGroundPanel.add(eventName);
			
		    eventField = new JTextField(); //  Event Name TextFeild
			eventField.setBounds(150, 50, 140, 27);
			backGroundPanel.add(eventField);
			
			JLabel categoryName = new JLabel("Category title :");   // Category type Label
		    categoryName.setBounds(30, 100, 110, 27);
		    categoryName.setFont(new Font("Serif",Font.BOLD,15));
			backGroundPanel.add(categoryName);
			
			categoryField = new JTextField(); //  Category type TextFeild
			categoryField.setBounds(150, 100, 140, 27);
			backGroundPanel.add(categoryField);
			
			
//	**********************//Button ( Add) ****************************
			
			addCategoryTitle = new JButton("Add");
			addCategoryTitle.setBounds(150, 150, 60, 27);
			addCategoryTitle.addActionListener(this);
			backGroundPanel.add(addCategoryTitle);
			
			
//			**********************//Button ( Add Sun Category) ****************************
			
			addSubCategory = new JButton("Add Subcategory");
			addSubCategory.setBounds(310, 50, 120, 27);
			addSubCategory.addActionListener(this);
			backGroundPanel.add(addSubCategory);	
			
			
			
//			**********************//Button ( Go back) ****************************
			   Action goBack = new AbstractAction("Go back") {
			
				public void actionPerformed(ActionEvent e) {
					
					createEvent.showWindow();
					CreateNewEvent.this.dispose();
					
								
							}
						};
			
			goback = new JButton(goBack);
			goback .setBounds(330, 270, 100, 27);
		//	goback .addActionListener(this);
			backGroundPanel.add(goback );			
		
		
		
		 // *********** JFrame option ************
 	    setUndecorated(false);  
 	    setLocationRelativeTo(null);
 	    setResizable(false);
 	  //setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);	// running window close 
 	    setVisible(true);	
		
	}
	
	
	
	public void hideWindow() {                  // hide window 
		this.setVisible(false);
	}
	
	public void showWindow() {					// show window
		this.setVisible(true);
	}
	
	public static void main(String[] args) {
		
     // new CreateNewEvent();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		
		
		Vector<String> event_input = new Vector<String>();
		Vector<String> category_input = new Vector<String>();
		
		event_input.add(eventField.getText());
		category_input.add(categoryField.getText());
		
		System.out.println(event_input.elementAt(0));
	
		
		
		if(e.getSource()== addSubCategory)
		{
			new CreateSubCategory(this);
			
		}
		
		else if(e.getSource() == addCategoryTitle)
		{
			
			
	       callDB.nonFixedEventnameCategoryName(event_input,category_input);
			
		    eventField.setText(null);
		    categoryField.setText(null);
			
		}
		
	
		
	
  }

}
