package NonfixedEventOption;

import java.awt.Font;
import java.awt.Dialog.ModalityType;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;

import com.toedter.calendar.JDateChooser;

import pakageBookingForm.fixedPakageBooking;
import sun.util.calendar.BaseCalendar.Date;
import EventPakageCreate.fixedPakageDate;
import Login_win.BackGround;
import Login_win.LoginWindow;
import Login_win.Login_window_db;
import SelectedEvent.fixedPakageDetail;
import SelectedEvent.selectedEvent;
public class DateSelection extends JDialog implements ActionListener {
	
	 JButton next, goBack;
	 selectedEvent packageInfo;
	 Login_window_db callDB;
	 selectedEvent packageInfo_Date;
	 String evName,pacName;
	 SelectFormNameToFill goBackToFormName ;
	 JDateChooser dateChooser;
	
    public DateSelection(SelectFormNameToFill dateSelect)
    {
		super(dateSelect , ModalityType.APPLICATION_MODAL);
		setTitle("Select Date");
		setSize(600, 500);
		dateSelect.dispose();
		goBackToFormName = dateSelect;
		
		  //***************** JPanel option *****************
		
		 ImageIcon backGroundImage = new ImageIcon(LoginWindow.class.getResource("/background/8.jpg"));
			BackGround backGroundPanel = new BackGround(backGroundImage); // adding background
		    backGroundPanel.setLayout(null);
	        add(backGroundPanel);

				  // ************** JLabel (Select Date) *******************
				  
				  JLabel date = new JLabel("Select booking date");   //Fixed Pakage Date Label
				  date.setBounds(200, 80, 200, 26);
				  date.setFont(new Font("Serif",Font.BOLD,21));
				  backGroundPanel.add( date);	
				  
		//************************* Date chooser***********************
				  
				  dateChooser = new JDateChooser();
				  dateChooser.setBounds(200, 120, 175, 25);
				  backGroundPanel.add( dateChooser);
				
				   
	  
		// *************************** JButton (Next)************
			
//				  Action nextAction = new AbstractAction("Next") {
//
//						public void actionPerformed(ActionEvent arg0) {
//						
//							new fixedPakageBooking(fixedPakageDate.this, packageDate);												
//						}
//					};
				 
				 next= new JButton("Next");
				 next.setBounds(205, 330, 70, 25);
				 next.setFont(new Font("Serif",Font.BOLD,15));
				 next.addActionListener(this);
				 backGroundPanel.add(next);
				  

				  
				  
				// *************************** JButton (Go back)************
					
				  Action goBackAction = new AbstractAction("Go back") {

						public void actionPerformed(ActionEvent arg0) {
						
							DateSelection.this.dispose();
							goBackToFormName.showWindow();
						}
					};
				 
				 goBack = new JButton(goBackAction);
				 goBack.setBounds(420, 330, 90, 25);
				 goBack.setFont(new Font("Serif",Font.BOLD,15));
				 goBack.addActionListener(this);
				 backGroundPanel.add(goBack);	
				   
	  
		 // *********** JFrame option ************
	    setUndecorated(false);  
	    setLocationRelativeTo(null);
	    setResizable(false);
	    setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);	
	    setVisible(true);
    	
    	
    	
    	
    	
    }
    
	public void hideWindow() {                  // hide window 
		this.setVisible(false);
	}
	
	public void showWindow() {					// show window
		this.setVisible(true);
	}
	
	public static void main(String[] args) {
		
	//	new DateSelection();

	}

	@Override
	public void actionPerformed(ActionEvent e) {

      if(e.getSource() == next)
      {  
    	 new FillUpForm(this);
    	 dateChooser.setDefaultLocale(null);
    	  
      }
      
      else if (e.getSource() == goBack)
      {
    	  
			DateSelection.this.dispose();
			goBackToFormName.showWindow();
    	  
      }
		
	}

}
