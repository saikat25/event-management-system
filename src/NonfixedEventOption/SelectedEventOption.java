package NonfixedEventOption;

import java.awt.Font;
import java.awt.Dialog.ModalityType;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import Login_win.BackGround;
import Login_win.LoginWindow;
import NonfixedEvent.NonfixedSelectedEvent;

public class SelectedEventOption extends JDialog implements ActionListener{

	
	JButton  createForm, showinformation,formFill,infoType, goBack;
	String selectedEventName;
	NonfixedSelectedEvent selectedEventPass;
	
	public SelectedEventOption( NonfixedSelectedEvent selectedEvent)
	{
		
		super(selectedEvent,ModalityType.MODELESS) ;
		setTitle("Selected Event");
		setSize(400, 500);
		selectedEvent.hideWindow();
		selectedEventPass = selectedEvent;
		
	    
	    //***************** JPanel option *****************
		
		 ImageIcon backGroundImage = new ImageIcon(LoginWindow.class.getResource("/background/8.jpg"));
		 BackGround backGroundPanel = new BackGround(backGroundImage); // adding background
		 backGroundPanel.setLayout(null);
	     add(backGroundPanel);
	     
	     
	     
		
	 	   JLabel eventSelection = new JLabel("Event selection");
	 	   eventSelection.setFont(new Font("Serif", Font.BOLD, 20));
	 	   eventSelection.setBounds(130, 50, 130, 30);
	 	   backGroundPanel.add(eventSelection);
	     
	     
	     
	     //***********************************Button(Create Form)*********************
		   
		    createForm = new JButton("Create Form");
		    createForm.setBounds(130, 155, 140, 27);
		    createForm.addActionListener(this);
		    backGroundPanel.add(createForm);
		   
		   
        //**********************************Button(Show information)*********************
		  
		   
		  showinformation = new JButton("Show info");
		 showinformation.setBounds(130, 195, 140, 27);
		showinformation.addActionListener(this);
	 		   backGroundPanel.add(showinformation);
	 		   
       //**********************************Button(Form fill up)*********************
		 		  
	 		   
	 		  formFill = new JButton("Fill Form");
	 		 formFill.setBounds(130, 235, 140, 27);
	 		formFill.addActionListener(this);
			backGroundPanel.add(formFill);	
			
				//******************************** Button (Go back)***************************
				  
	 		   Action goBackAction = new AbstractAction("Go back") {
	 
	 			public void actionPerformed(ActionEvent e) {
	 					
	 				SelectedEventOption.this.dispose();
	 				selectedEvent.showWindow();
	 			}
	 		};
			  goBack = new JButton(goBackAction);
			  goBack.setBounds(300, 420, 80, 27);
			  goBack.addActionListener(this);
			   backGroundPanel.add( goBack);
			

 		
		 // *********** JFrame option ************
	    setUndecorated(false);  
	    setLocationRelativeTo(null);
	    setResizable(false);
	  //setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);	// running window close 
	    setVisible(true);	
		
		
		
	}
	
	
	public void hideWindow() {                  // hide window 
		this.setVisible(false);
	}
	
	public void showWindow() {					// show window
		this.setVisible(true);
	}
	
	public static void main(String[] args) {
		
		//new SelectedEventOption();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		
		
		 if(e.getSource() == createForm)
		{

			new CreateNewForm(selectedEventPass,this);
			
		}
		
		else if (e.getSource() == showinformation)
		{
			
			new SelectTypeOfInformation(this,selectedEventPass);
			
		}
		
		else if(e.getSource() == formFill)
		{
			
			new SelectFormNameToFill(this);
			
		}
		 
	
	
	}

}
