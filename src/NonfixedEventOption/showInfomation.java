package NonfixedEventOption;

import java.awt.Font;
import java.awt.Dialog.ModalityType;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import Login_win.BackGround;
import Login_win.LoginWindow;
import NonfixedEvent.NonfixedSelectedEvent;

public class showInfomation extends JDialog implements ActionListener {

	JLabel  addLabel;
	JButton goBack,delete;

	public showInfomation(SelectTypeOfInformation eventInfo,NonfixedSelectedEvent selectedEventAdd)
	{
		
		
		
		
		 super( eventInfo,ModalityType.MODELESS) ;
			setTitle("Event Information");
			setSize(450, 550);
			eventInfo.hideWindow();
			
			  //***************** JPanel option *****************
			
		    ImageIcon backGroundImage = new ImageIcon(LoginWindow.class.getResource("/background/8.jpg"));
			BackGround backGroundPanel = new BackGround(backGroundImage); // adding background
		    backGroundPanel.setLayout(null);
	        add(backGroundPanel);
	        
	        
	        
			  // ************** JLabel (Add Sub Category) *******************
	       addLabel = new JLabel("Event Information");   //Create NEw Form Label
	       addLabel.setBounds(150, 30, 200, 25);
	       addLabel.setFont(new Font("Serif",Font.BOLD,20));
			  backGroundPanel.add( addLabel);
			  
				//******************************** Button (Delete)***************************
			  
				delete = new JButton("Delete Entry");
			    delete .setBounds(200, 450, 110, 27);
				delete .addActionListener(this);
			    backGroundPanel.add(  delete );			   
				  
			  
			  
			  
		//******************************** Button (Go back)***************************
	
			  
	 		   Action goBackAction = new AbstractAction("Go back") {
	 
	 			public void actionPerformed(ActionEvent e) {
	 					
	 				showInfomation.this.dispose();
	 				eventInfo.showWindow();
	 			}
	 		};
			  
			  
			  goBack = new JButton(goBackAction);
			  goBack.setBounds(340, 450, 80, 27);
			  goBack.addActionListener(this);
			   backGroundPanel.add( goBack);	  
	      

		 // *********** JFrame option ************
	    setUndecorated(false);  
	    setLocationRelativeTo(null);
	    setResizable(false);
	   // setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);	
	    setVisible(true);
		
	}
	
	
	
	public void hideWindow() {                  // hide window 
		this.setVisible(false);
	}
	
	public void showWindow() {					// show window
		this.setVisible(true);
	}
	
	
	public static void main(String[] args) {
	
	//	new showInfomation();

	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		
		if(arg0.getSource() == delete)
		{
			
			//String deleteEvent = this.selectEvent();
			//JOptionPane.showConfirmDialog(deleteEvent, , // confirm box
				//"Confirmation Window",JOptionPane.YES_NO_OPTION);
			
			JOptionPane.showConfirmDialog(delete, "Do you want to delete the booking ? ", "Confirmation Window", JOptionPane.YES_NO_OPTION);
			
		}
		
		
	}

}
