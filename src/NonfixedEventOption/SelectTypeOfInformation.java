package NonfixedEventOption;

import java.awt.Font;
import java.awt.Dialog.ModalityType;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import Login_win.BackGround;
import Login_win.LoginWindow;
import Login_win.Login_window_db;
import NonfixedEvent.NonfixedSelectedEvent;

public class SelectTypeOfInformation extends JDialog implements ActionListener {
	JLabel selectInfoTypeLabel ;
	JComboBox<String> infoTypeList ;
	Vector<String> infoTypeName = new Vector<String>();
	JButton infoType,goBack ;
	Login_window_db callDB = new Login_window_db();
	NonfixedSelectedEvent selectedEventAdd;
	
	public SelectTypeOfInformation( SelectedEventOption infoTypeSelection,NonfixedSelectedEvent selectedEventPass)
	{
		super(infoTypeSelection,ModalityType.MODELESS) ;
		setTitle("Select Form Name");
		setSize(450, 550);
		infoTypeSelection.hideWindow();
		selectedEventAdd= selectedEventPass;
	//	createFormWindowPass = fillForm; 
		
		  //***************** JPanel option *****************
		
	    ImageIcon backGroundImage = new ImageIcon(LoginWindow.class.getResource("/background/8.jpg"));
		BackGround backGroundPanel = new BackGround(backGroundImage); // adding background
	    backGroundPanel.setLayout(null);
        add(backGroundPanel);
        
        // ************** JLabel (Select Form name) *******************
        selectInfoTypeLabel = new JLabel("Select Information Type");   //Create NEw Form Label
        selectInfoTypeLabel.setBounds(130, 30, 200, 25);
        selectInfoTypeLabel.setFont(new Font("Serif",Font.BOLD,17));
 		  backGroundPanel.add(selectInfoTypeLabel);
		
 		 infoTypeName.add("Event Information");
 		 infoTypeName.add("Form Infromation");
 		  
 		  
 		  
 		  infoTypeList = new JComboBox<String>(infoTypeName);
		  infoTypeList.setBounds(150, 77, 160, 25);
		  backGroundPanel.add( infoTypeList) ;
		  infoTypeList.setSelectedItem(null);
		  infoTypeList.addActionListener(this);
		  
		  
			//******************************** Button (Sub Actegory)***************************
		  
		   infoType = new JButton("Next");
		   infoType.setBounds(200, 450, 60, 27);
		   infoType.addActionListener(this);
		   backGroundPanel.add( infoType);
		   
		   
		   
			//******************************** Button (Go back)***************************
			  
 		   Action goBackAction = new AbstractAction("Go back") {
 
 			public void actionPerformed(ActionEvent e) {
 					
 				 SelectTypeOfInformation.this.dispose();
 				infoTypeSelection.showWindow();
 			}
 		};
		  goBack = new JButton(goBackAction);
		  goBack.setBounds(330, 450, 80, 27);
		  goBack.addActionListener(this);
		   backGroundPanel.add( goBack);
		
		
		
		
		 // *********** JFrame option ************
	    setUndecorated(false);  
	    setLocationRelativeTo(null);
	    setResizable(false);
	   // setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);	
	    setVisible(true);	
		
		
		
	}
	
	public void hideWindow() {                  // hide window 
		this.setVisible(false);
	}
	
	public void showWindow() {					// show window
		this.setVisible(true);
	}
	
	
	
	public static void main(String[] args) {
	
		
		// new SelectTypeOfInformation(this);

	}
	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == infoType)
		{
			if(infoTypeList.getSelectedItem() == infoTypeName.elementAt(0))
			{
				
				new showInfomation(this,selectedEventAdd);
				
			}
			else if (infoTypeList.getSelectedItem() == null)
			{
				
				JOptionPane.showMessageDialog (null, "Select a type from the list", "Error Message", JOptionPane.ERROR_MESSAGE);    
			}
			else  
			{
				new showFormInformation(SelectTypeOfInformation.this);
				
			}
			

			
		}

		
	}

}
