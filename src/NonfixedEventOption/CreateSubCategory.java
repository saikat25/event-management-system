package NonfixedEventOption;

import java.awt.Font;
import java.awt.Dialog.ModalityType;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

import javafx.scene.control.ScrollBar;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;

import com.sun.org.apache.bcel.internal.generic.RETURN;

import Login_win.BackGround;
import Login_win.LoginWindow;
import Login_win.Login_window_db;

public class CreateSubCategory extends JDialog implements ActionListener  {

	 JLabel addLabel,categoryTitle,subCategoryName,Eventname;
	 JComboBox<String >  categoryTitleList,EventList;
	 JTextArea subCategoryInput;
	 JButton  addSubCategoryName , backToPrevious,add;
	 Login_window_db callDB = new Login_window_db();
	 Vector<String> categoryList = new Vector<String>();
	 Vector<String> EventNameList = new Vector<String>();
	 String selectecdEvent;
	 String name;
	
	public CreateSubCategory(CreateNewEvent addSubCategory)
	
	{
		
		super(addSubCategory,ModalityType.MODELESS) ;
		setTitle("Create New Event");
		setSize(450, 550);
		addSubCategory.hideWindow();
		
		
	    
	    //***************** JPanel option *****************
		
		    ImageIcon backGroundImage = new ImageIcon(LoginWindow.class.getResource("/background/8.jpg"));
			BackGround backGroundPanel = new BackGround(backGroundImage); // adding background
		    backGroundPanel.setLayout(null);
	        add(backGroundPanel);
	    
			  // ************** JLabel (Add Sub Category) *******************
	         addLabel = new JLabel("Add Sub Category");   //Create Fixed Pakage category title Label
	         addLabel.setBounds(150, 30, 200, 25);
	         addLabel.setFont(new Font("Serif",Font.BOLD,20));
			  backGroundPanel.add( addLabel);  
	        
	        
	        //************************** Add Required Label ****************
			  
			    Eventname = new JLabel("Event Name : ");   //Create Fixed Pakage category title Label
			    Eventname.setBounds(30, 80, 120, 15);
			    Eventname.setFont(new Font("Serif",Font.BOLD,15));
				 backGroundPanel.add( Eventname);
				  
				  
				  EventNameList= callDB.getNonFixedEventList();
				  
				  EventList = new JComboBox<String>(EventNameList);
				  EventList.setBounds(180, 77, 180, 25);
				  backGroundPanel.add( EventList) ;
				  EventList.setSelectedItem(null);
				  EventList.addActionListener(this);
			  
			  
			  
			  categoryTitle = new JLabel("Category Title : ");   //Create Fixed Pakage category title Label
			  categoryTitle.setBounds(30, 120, 200, 20);
			  categoryTitle.setFont(new Font("Serif",Font.BOLD,15));
			  backGroundPanel.add( categoryTitle);
			  
			  
			//  selectecdEvent = getSelectedEvent();
			  
			  
			  categoryTitleList = new JComboBox<String>(getSelectedEvent());
			  categoryTitleList.setBounds(180,122,180,25);
			  backGroundPanel.add( categoryTitleList);
			  categoryTitleList.setSelectedItem(null);
			  
			  subCategoryName = new JLabel("Subcategory Name : ");   //Create Fixed Pakage category title Label
			  subCategoryName.setBounds(30, 160, 200, 20);
			  subCategoryName.setFont(new Font("Serif",Font.BOLD,15));
			  backGroundPanel.add(subCategoryName);
			  
			  
			  subCategoryInput = new JTextArea();   //Create Fixed Pakage category title Label
			 // subCategoryInput.setBounds(50, 100, 200, 100);
			  subCategoryInput.setFont(new Font("Serif",Font.PLAIN,17));
			  JScrollPane scrollPane = new JScrollPane(subCategoryInput); // Add Scroll bar to Text area
			  scrollPane.setBounds(180,162,180,150);
			  scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
			  backGroundPanel.add(scrollPane);
			  
			  
		//******************************** Button (Sub Actegory)***************************
			  
			   addSubCategoryName = new JButton("Add");
			   addSubCategoryName.setBounds(300, 340, 60, 27);
			   addSubCategoryName.addActionListener(this);
			   backGroundPanel.add( addSubCategoryName);
			   
			   
			   

			   
			   
			   
			 //******************************** Button (Go Back)***************************
				
			   Action goBack = new AbstractAction("Go back")
			   {
				   public void actionPerformed(ActionEvent e) {
					   
					CreateSubCategory.this.dispose();
					addSubCategory.showWindow();
	 		
	 			} 
				   
   
			   };
			   
			   
			   backToPrevious = new JButton(goBack);
			   backToPrevious.setBounds(260, 440, 100, 27);
			  // addSubCategoryName.addActionListener(this);
			   backGroundPanel.add( backToPrevious);		   
		
		
		
		

		 // *********** JFrame option ************
	    setUndecorated(false);  
	    setLocationRelativeTo(null);
	    setResizable(false);
	   // setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);	
	    setVisible(true);
	}
	
	
	public void hideWindow() {                  // hide window 
		this.setVisible(false);
	}
	
	public void showWindow() {					// show window
		this.setVisible(true);
	}
	
	public Vector<String> getSelectedEvent()
	{
		return categoryList ;
	}

	
	public static void main(String[] args) {
		

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		
		Vector<String> eventName_category = new Vector<String>();
		Vector<String> subcategoryList = new Vector<String>();
	
	if ( e.getSource() == EventList )
		
		{
			
			if( EventList.getSelectedItem().toString() != null )
				
			{
				categoryTitleList.setSelectedItem(null);
			    categoryTitleList.removeAllItems();
				
			}
			
			selectecdEvent = EventList.getSelectedItem().toString();
			System.out.println(selectecdEvent);
			categoryList= callDB.getNonFixedCatrgoryList(selectecdEvent);
			
			categoryTitleList.addItem(null);
			int i ;
			for(i=0;i<categoryList.size();i++)
			{
				//categoryTitleList.setSelectedItem(null);
				categoryTitleList.addItem(categoryList.elementAt(i));
				System.out.println(categoryList.elementAt(i));
				
			}
	
			
		}
		
		else if (e.getSource() ==  addSubCategoryName)
		{
			
			
			
		  
			eventName_category.add( EventList.getSelectedItem().toString());
			eventName_category.add(categoryTitleList.getSelectedItem().toString());
			// Split the queries into multiple strings
			String[] queries = subCategoryInput.getText().split(";");
			
			
			// execute each query in a loop
		    for (String query : queries) {
		    
		    	subcategoryList.add(query.trim());
		    	//System.out.println(subcategoryList.elementAt(i));
		    	
		    }
		    
		    
			
			categoryTitleList.setSelectedItem(null);
			subCategoryInput.setText(null);

			
			
		  callDB.nonFixedEventname_SubCategoryName(eventName_category, subcategoryList);
		  callDB.nonFixedEvent_Category_SubCategoryName(eventName_category, subcategoryList);
			
		}
		
	//	new CreateSubCategory();
	}

}
