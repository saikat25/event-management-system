package NonfixedEventOption;

import java.awt.Font;
import java.awt.Dialog.ModalityType;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JTextField;

import Login_win.BackGround;
import Login_win.LoginWindow;
import NonfixedEvent.NonfixedSelectedEvent;

public class FillUpForm extends JDialog implements ActionListener {
	
	JLabel  addLabel,addFormNameLabel;
	JButton goBack,  Done , delete , addNewInfoLabel;
	JTextField addNewinfo;
	
	
	 public FillUpForm(DateSelection formName)
	 {
		 super(formName,ModalityType.MODELESS) ;
			setTitle("Create New Form");
			setSize(450, 550);
			formName.hideWindow();
			
			  //***************** JPanel option *****************
			
		    ImageIcon backGroundImage = new ImageIcon(LoginWindow.class.getResource("/background/8.jpg"));
			BackGround backGroundPanel = new BackGround(backGroundImage); // adding background
		    backGroundPanel.setLayout(null);
	        add(backGroundPanel);
	        
	        
	        
	        // ************** JLabel (Add Form name) *******************
	        
		       addFormNameLabel = new JLabel("Registration");   //Create NEw Form Label
		       addFormNameLabel.setBounds(170, 30, 200, 25);
		       addFormNameLabel.setFont(new Font("Serif",Font.BOLD,20));
				  backGroundPanel.add(addFormNameLabel);
	        
				  
			  // ************** JLabel (Add Form) *******************
	       addLabel = new JLabel("Form");   //Create NEw Form Label
	       addLabel.setBounds(200, 60, 200, 25);
	       addLabel.setFont(new Font("Serif",Font.BOLD,20));
			  backGroundPanel.add( addLabel); 
			  
			  
		   //***************************** Add New Info Title *********************
			  
			  addNewinfo = new JTextField();
			  addNewinfo.setBounds(30, 400, 130, 25);
			  backGroundPanel.add(  addNewinfo);
			  
			  
			  
			  addNewInfoLabel = new JButton("Add new info");   //Create form name  Label
			  addNewInfoLabel.setBounds(30, 450, 110, 27);
			  addNewInfoLabel.setFont(new Font("Serif",Font.PLAIN,15));
			  backGroundPanel.add(addNewInfoLabel);
			  
	
			  
				//******************************** Button (Done)***************************
			  
			  Done  = new JButton("Fill up Form");
			  Done .setBounds(200, 450, 110, 27);
			  Done .addActionListener(this);
			   backGroundPanel.add(   Done );	
	  
		//******************************** Button (Go back)***************************
			  
	 		   Action goBackAction = new AbstractAction("Go back") {
	 
	 			public void actionPerformed(ActionEvent e) {
	 					
	 				FillUpForm.this.dispose();
	 				formName.showWindow();
	 			}
	 		};
			  goBack = new JButton(goBackAction);
			  goBack.setBounds(330, 450, 80, 27);
			  goBack.addActionListener(this);
			   backGroundPanel.add( goBack);	  
	      

		 // *********** JFrame option ************
	    setUndecorated(false);  
	    setLocationRelativeTo(null);
	    setResizable(false);
	   // setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);	
	    setVisible(true); 
	 }

	 
		
		public void hideWindow() {                  // hide window 
			this.setVisible(false);
		}
		
		public void showWindow() {					// show window
			this.setVisible(true);
		}
		
	public static void main(String[] args) {
		//new FillUpForm();

	}

	@Override
	public void actionPerformed(ActionEvent e) {
	
		
	}

}
