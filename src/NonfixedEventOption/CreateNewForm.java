package NonfixedEventOption;

import java.awt.Font;
import java.awt.Dialog.ModalityType;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;

import Login_win.BackGround;
import Login_win.LoginWindow;
import Login_win.Login_window_db;
import NonfixedEvent.NonfixedSelectedEvent;

public class CreateNewForm extends JDialog implements ActionListener {

	
	
	JLabel addLabel,formNameLabel,forminfoTitleLabel;
	JTextField formName;
	JTextArea formInfoList;
	JButton createForm, backToPrevious,fillUpForm;
	SelectedEventOption createFormWindowPass;
	Login_window_db callDB = new Login_window_db();
	NonfixedSelectedEvent selectedEventAdd;
	

	
	public CreateNewForm(NonfixedSelectedEvent selectedEventPass,SelectedEventOption createFormWindow) {
		
		super(createFormWindow,ModalityType.MODELESS) ;
		setTitle("Create New Form");
		setSize(450, 550);
		createFormWindow.hideWindow();
		createFormWindowPass = createFormWindow; 
		selectedEventAdd=selectedEventPass;
		
		  //***************** JPanel option *****************
		
	    ImageIcon backGroundImage = new ImageIcon(LoginWindow.class.getResource("/background/8.jpg"));
		BackGround backGroundPanel = new BackGround(backGroundImage); // adding background
	    backGroundPanel.setLayout(null);
        add(backGroundPanel);
        
        
        
		  // ************** JLabel (Add Sub Category) *******************
       addLabel = new JLabel("Create New Form");   //Create NEw Form Label
       addLabel.setBounds(150, 30, 200, 25);
       addLabel.setFont(new Font("Serif",Font.BOLD,20));
		  backGroundPanel.add( addLabel);  
      
      
      
		  formNameLabel = new JLabel("From Name : ");   //Create form name  Label
		  formNameLabel.setBounds(30, 80, 120, 15);
		  formNameLabel.setFont(new Font("Serif",Font.BOLD,15));
		  backGroundPanel.add( formNameLabel);
		  
		  
		  formName = new JTextField();
		  formName.setBounds(180, 77, 180, 25);
		  backGroundPanel.add(  formName);
		  
		  
		  forminfoTitleLabel = new JLabel("Form info Title : ");   //  Create Fixed Pakage category title Label
		  forminfoTitleLabel.setBounds(30, 150, 200, 20);
		  forminfoTitleLabel.setFont(new Font("Serif",Font.BOLD,15));
		  backGroundPanel.add(forminfoTitleLabel);
		  
		  
		  formInfoList = new JTextArea();   //  Create Fixed Pakage category title Label
		//  formInfoList.setBounds(50, 100, 200, 100);
		  formInfoList.setFont(new Font("Serif",Font.PLAIN,15));
		  JScrollPane scrollPane = new JScrollPane(formInfoList); // Add Scroll bar to Text area
		  scrollPane.setBounds(180,152,180,150);
		  scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		  backGroundPanel.add(scrollPane);
		  	  
		  
		  
	//******************************** Button (Sub Actegory)***************************
		  
		  createForm = new JButton("Create Form");
		  createForm.setBounds(260, 420, 100, 27);
		  createForm.addActionListener(this);
		   backGroundPanel.add( createForm);
		   
		   
		 //******************************** Button (Go Back)***************************
			
		   Action goBack = new AbstractAction("Go back")
		   {
			   public void actionPerformed(ActionEvent e) {
				   
				CreateNewForm.this.dispose();
				createFormWindow.showWindow();
 		
 			} 
			   

		   };
		   
		   
		   backToPrevious = new JButton(goBack);
		   backToPrevious.setBounds(280, 480, 80, 27);
		  // addSubCategoryName.addActionListener(this);
		   backGroundPanel.add( backToPrevious);		   
	
		   

		
		
		 // *********** JFrame option ************
	    setUndecorated(false);  
	    setLocationRelativeTo(null);
	    setResizable(false);
	   // setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);	
	    setVisible(true);
	}



	public void hideWindow() {                  // hide window 
		this.setVisible(false);
	}
	
	public void showWindow() {					// show window
		this.setVisible(true);
	}
	

	
	public static void main(String[] args) {
		
	//	new CreateNewForm();

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		
		Vector<String> formInfo = new Vector<String>();
		int formNum = 1;
 
		
		if(e.getSource()== createForm )
		{
			
			String selectedEvent = selectedEventAdd.selectEvent();
			System.out.println(selectedEvent);
			
			String FormName = formName.getText();
			
			
			// Split the queries into multiple strings
			String[] queries = formInfoList.getText().split(",");
			
			
			// execute each query in a loop
		    for (String query : queries) {
		    
		    	formInfo.add(query.trim());
		    	//System.out.println(subcategoryList.elementAt(i));
		    	
		    }
			
			
		    callDB.nonFixedEvent_CreateNewForm(selectedEvent,formNum,FormName, formInfo);
		    
			formName.setText(null);
			formInfoList.setText(null);
		    
		    formNum++;
		}
		
		
		
	}

}
