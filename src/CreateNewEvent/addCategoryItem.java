package CreateNewEvent;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import Login_win.BackGround;
import Login_win.LoginWindow;
import Login_win.Login_window_db;
import SelectedEvent.createNewEvent;

public class addCategoryItem extends JDialog implements ActionListener{
	
	//JPanel backGroundPanel = new JPanel();
	
	Login_window_db callDB = new Login_window_db();
	Vector<String> eL = new Vector<String>();
	Vector<String> cTL = new Vector<String>();
	Vector<String> categoryList = new Vector<String>();
	JComboBox<String> eventList , categoryTitleList;
	JButton addCategoryItem;
	JTextField newCategoryItem;
	String selectecdEvent;
	
	
    public addCategoryItem(createNewEvent CategoryItem) {
    	
    	super(CategoryItem, ModalityType.APPLICATION_MODAL);
		setTitle("Add category Item");
		setSize(400, 300);
		//CategoryItem.hideWindow();
		
		
		  //***************** JPanel option *****************
		
		   ImageIcon backGroundImage = new ImageIcon(LoginWindow.class.getResource("/background/8.jpg"));
			BackGround backGroundPanel = new BackGround(backGroundImage); // adding background
		    backGroundPanel.setLayout(null);
	        add(backGroundPanel);
		//addCategoryItemPanel.setLayout(null);
		//addCategoryItemPanel.setBackground(Color.CYAN);
		//add(addCategoryItemPanel);
		
		//***************** JLable option **********************************
		
		JLabel eventName = new JLabel("Event name :");            // Event Name Label
		eventName.setBounds(50, 50, 110, 25);
		eventName.setFont(new Font("Serif",Font.BOLD,15));
		backGroundPanel.add(eventName);
		
		eL= callDB.addEventToSelectList();        // store event name from database 
		
		eventList = new JComboBox(eL);    // Event List 
		eventList.setBounds(180, 50, 140, 25);
		backGroundPanel.add(eventList);
		eventList.setSelectedItem(null);
		
		JLabel categoryTitle = new JLabel("Category title :");            // Category Title Label
		categoryTitle.setBounds(50, 100, 110, 25);
		categoryTitle.setFont(new Font("Serif",Font.BOLD,15));
		backGroundPanel.add(categoryTitle);
		
	    cTL= callDB.addCategoryToSelectList();        // store CategoryList from database
		
		categoryTitleList = new JComboBox(cTL);    // Category Title List 
		categoryTitleList.setBounds(180, 100, 140, 25);
		backGroundPanel.add(categoryTitleList);
		categoryTitleList.setSelectedItem(null);
		
		
		JLabel categoryItem = new JLabel("Category item :");            // Category Item Label
		categoryItem.setBounds(50, 150, 110, 25);
		categoryItem.setFont(new Font("Serif",Font.BOLD,15));
		backGroundPanel.add(categoryItem);
		
		 newCategoryItem = new JTextField();                  // Category Item Name 
		 newCategoryItem.setBounds(180, 150, 140, 25);
		backGroundPanel.add(newCategoryItem);
		
		// ***************************** Add Category Item Button *******************
//		
//		   Action addCategoryItemAction = new AbstractAction("Add item") {
//
//				public void actionPerformed(ActionEvent arg0) {
//				
//					addCategoryItem.this.dispose();
//					CategoryItem.showWindow();
//				}
//			};
	
	
	addCategoryItem = new JButton("Add item");
	addCategoryItem.setBounds(220, 220, 100, 25);
	addCategoryItem.addActionListener(this);
	backGroundPanel.add(addCategoryItem);
		
		   
		   
			 // *********** JFrame option ************
		    setUndecorated(false);  
		    setLocationRelativeTo(null);
		    setResizable(false);
		//    setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);	
		    setVisible(true);
		
	}
	
    
    
	
	public void hideWindow() {                  // hide window 
		this.setVisible(false);
	}
	
	public void showWindow() {					// show window
		this.setVisible(true);
	}
	
	public Vector<String> getSelectedEvent()
	{
		return categoryList ;
	}

	public static void main(String[] args) {
	 
	//	new addCategoryItem();

	}


	@Override
	public void actionPerformed(ActionEvent event) {
	
		
		Vector<String> newCategory = new Vector<String>();
		String selectedItem = eventList.getSelectedItem().toString(); 
		String selectedCategoryTitle = categoryTitleList.getSelectedItem().toString(); 

	boolean dbresponse;

	 if(event.getSource() == addCategoryItem)
	{
	    
		newCategory.add(selectedItem);
		newCategory.add(selectedCategoryTitle);
	    newCategory.add(newCategoryItem.getText()) ;
		
		dbresponse= callDB.addNewCategoryItem(newCategory ) ;
	
		//this.CategoryTitle.showWindow();
		addCategoryItem.this.dispose();
		System.out.println("ele " +newCategory.elementAt(0));
		
	}
	 
	 
	
		
	}

}
