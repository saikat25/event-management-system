package CreateNewEvent;

import java.awt.Color;
import java.awt.Dialog.ModalityType;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import Login_win.BackGround;
import Login_win.LoginWindow;
import Login_win.Login_window_db;
import SelectedEvent.createNewEvent;

public class selectEvent_showinfo extends JDialog  implements ActionListener{
	
//	JPanel backGroundPanel = new JPanel(); 
	Login_window_db callDB = new Login_window_db();
	Vector<String> eL = new Vector<String>();
	 JComboBox<String> eventList;
	 JButton ok;
	 createNewEvent selectEventpPass;
	 String selectedItem;
	
	
	
	 public selectEvent_showinfo(createNewEvent selectEvent)
	 {
		 super(selectEvent,ModalityType.APPLICATION_MODAL);
		setTitle("Select event ");
		 setSize(400, 300);
		 selectEvent.hideWindow();
		 selectEventpPass=selectEvent;
		 
		  //***************** JPanel option *****************
		 
		 ImageIcon backGroundImage = new ImageIcon(LoginWindow.class.getResource("/background/8.jpg"));
			BackGround backGroundPanel = new BackGround(backGroundImage); // adding background
		    backGroundPanel.setLayout(null);
	        add(backGroundPanel);
		// selectPanel.setLayout(null);
		// selectPanel.setBackground(Color.CYAN);
	   //  add(selectPanel);
	     
	     
	     //**************** JLabel option ***************
	     
	     JLabel eventSelect = new JLabel("Select Event");   // Select Label
	     eventSelect.setBounds(150, 25, 150, 25);
	     eventSelect.setFont(new Font("Serif",Font.BOLD,20));
	     backGroundPanel.add(eventSelect);
	     
	     
	     eL= callDB.addEventToSelectList();        // store event name from database 
	     
	     
	     eventList = new JComboBox<String>(eL);
	     eventList.setBounds(135, 75, 150, 25);
	     backGroundPanel.add(eventList);
	     
	     //  ***************** Ok Button *************
	     
//		   Action okAction = new AbstractAction("OK") {
//
//				public void actionPerformed(ActionEvent arg0) {
//				
//				
//				
//					
//				}
//			};
	
	
	 ok = new JButton("OK");
	 ok.setBounds(215,200, 70, 25);
	 ok.setFont(new Font("Serif",Font.BOLD,13));
	 ok.addActionListener(this);
	 backGroundPanel.add( ok);
	     
		 
		 
		 
		 
		 // *********** JFrame option ************
		    setUndecorated(false);  
		    setLocationRelativeTo(null);
		    setResizable(false);
		    setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);	
		    setVisible(true);
		 
		 
		 
		 
	 }
	 
		public void hideWindow() {                  // hide window 
			this.setVisible(false);
		}
		
		public void showWindow() {					// show window
			this.setVisible(true);
		}
	 
	 
	 public String selectEvent()
	 {
	    selectedItem = (eventList.getSelectedItem().toString() );
		
		return selectedItem;
	 }

	public static void main(String[] args) {
		// new selectEvent_showinfo();
		 

	}

	@Override
	public void actionPerformed(ActionEvent event) {
		
		if(event.getSource() == ok)
		{
			
			new showEventInfo(selectEvent_showinfo.this, selectEventpPass);
	     	selectEvent_showinfo.this.dispose();
	     	
			
		}
		
		
	
		
	}

}
