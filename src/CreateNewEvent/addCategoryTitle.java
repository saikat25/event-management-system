package CreateNewEvent;

import java.awt.Color;
import java.awt.Font;
import java.awt.Dialog.ModalityType;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import Login_win.BackGround;
import Login_win.LoginWindow;
import Login_win.Login_window_db;
import PostLogin_win.AfterLogin;
import SelectedEvent.createNewEvent;

public class addCategoryTitle extends JDialog implements ActionListener{
	
	
	//JPanel  backGroundPanel = new JPanel();
	Login_window_db callDB = new Login_window_db();
	JComboBox eventList ;
	JTextField newCategoryItem ;
	JButton addCategoryTitle;
	Vector<String> eL = new Vector<String>();

       public addCategoryTitle ( createNewEvent CategoryTitle){
    	   
    	   
    	super(CategoryTitle, ModalityType.APPLICATION_MODAL);
   		setTitle("Create New CategoryTitle");
   		//CategoryTitle.hideWindow();
		setSize(400, 300);
		
	
		
		  //***************** JPanel option *****************
		
		 ImageIcon backGroundImage = new ImageIcon(LoginWindow.class.getResource("/background/8.jpg"));
			BackGround backGroundPanel = new BackGround(backGroundImage); // adding background
		    backGroundPanel.setLayout(null);
	        add(backGroundPanel);
		
		
		// backGroundPanel.setLayout(null);
		// backGroundPanel.setBackground(Color.CYAN);
		//add( backGroundPanel);
		
		
		JLabel eventName = new JLabel("Event name :");            // Event Name Label
		eventName.setBounds(50, 50, 110, 25);
		eventName.setFont(new Font("Serif",Font.BOLD,15));
		backGroundPanel.add(eventName);
		
		eL= callDB.addEventToSelectList();        // store event name from database 
		
		eventList = new JComboBox<String>(eL);    // Event List 
		eventList.setBounds(180, 50, 140, 25);
		 backGroundPanel.add(eventList);
		
		
		JLabel categoryItem = new JLabel("Category title :");            // Category Item Label
		categoryItem.setBounds(50, 100, 110, 25);
		categoryItem.setFont(new Font("Serif",Font.BOLD,15));
		 backGroundPanel.add(categoryItem);
		
		newCategoryItem = new JTextField();                  // Category Item Name 
		newCategoryItem.setBounds(180, 100, 140, 25);
		 backGroundPanel.add(newCategoryItem);
		
		// ***************************** Add Category Item Button *******************
		
//		   Action addCategoryItemAction = new AbstractAction("Add title") {
//
//				public void actionPerformed(ActionEvent arg0) {
//				
//					addCategoryTitle.this.dispose();
//					CategoryTitle.showWindow();
//				}
//			};
	
	
	addCategoryTitle = new JButton("Add title");
	addCategoryTitle.setBounds(220, 220, 100, 25);
	addCategoryTitle.addActionListener(this);
	backGroundPanel.add(addCategoryTitle);
		
		   
		   
			 // *********** JFrame option ************
		    setUndecorated(false);  
		    setLocationRelativeTo(null);
		    setResizable(false);
		    setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);	// In case of JDialouge EXIT_ON _CLOSE 
		   														// dosen't use(running window)
		    setVisible(true);
		
	}
       


	public static void main(String[] args) {
	
	//	new addCategoryTitle();   // It doesn't in case of JDialouge
	}


	@Override
	public void actionPerformed(ActionEvent event) {
		
		Vector<String> newCategory = new Vector<String>();
		String selectedItem = eventList.getSelectedItem().toString(); 
		
		
		newCategory.add(selectedItem);	
	    newCategory.add(newCategoryItem.getText()) ;

	
	boolean dbresponse;
	
	
		
	 if(event.getSource() == addCategoryTitle)
	{
	    
		
		dbresponse= callDB.addNewCategoryTitle(newCategory ) ;
	
		//this.CategoryTitle.showWindow();
		addCategoryTitle.this.dispose();
		System.out.println("ele " +newCategory.elementAt(0));
		
	}
	
	
		
		
	}

}
