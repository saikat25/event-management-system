package CreateNewEvent;

import java.awt.Color;
import java.awt.Dialog.ModalityType;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;

import Login_win.BackGround;
import Login_win.LoginWindow;
import Login_win.Login_window_db;
import SelectedEvent.createNewEvent;

public class showEventInfo extends JDialog implements ActionListener {
	
	//JPanel backGroundPanel = new JPanel();
	Vector<String> cTL = new Vector<String>();
	Login_window_db callDB = new Login_window_db();
	JComboBox categoryTitleList,categoryItemList;
	String eventLabel,selectedTitle;
	JTextArea categoryItemForCT;
	JButton goBack;
	Vector<String> selectedItem = new Vector<String>();
	

	public showEventInfo(selectEvent_showinfo showEventInfo, createNewEvent selectEventpPass) {
		super(showEventInfo, ModalityType.APPLICATION_MODAL);	
    	setTitle("Show Event Info");
		setSize(600, 500);
		showEventInfo.hideWindow();
		
		  //***************** JPanel option *****************
		
		 ImageIcon backGroundImage = new ImageIcon(LoginWindow.class.getResource("/background/8.jpg"));
			BackGround backGroundPanel = new BackGround(backGroundImage); // adding background
		    backGroundPanel.setLayout(null);
	        add(backGroundPanel);
		
		
		//showInfoPanel.setLayout(null);
		//showInfoPanel.setBackground(Color.CYAN);
		//add(showInfoPanel);
		
		
	//************ JLable option **************
	        
	       
		
		JLabel eventName = new JLabel("Event name :");   // Event Name Label
		eventName.setBounds(50, 50, 110, 25);
		eventName.setFont(new Font("Serif",Font.BOLD,15));
		backGroundPanel.add(eventName);
		
	    eventLabel = showEventInfo.selectEvent(); 
		
		JTextField eventField = new JTextField(); //  Event Name TextFeild
		eventField.setBounds(170, 50, 140, 25);
		backGroundPanel.add(eventField);
		eventField.setText(eventLabel);
		
		JLabel categoryName = new JLabel("Category title :");   // Category type Label
	    categoryName.setBounds(50, 100, 110, 25);
	    categoryName.setFont(new Font("Serif",Font.BOLD,15));
	    backGroundPanel.add(categoryName);
		
	    cTL = callDB.categoryTitleForSelectedEvent(eventLabel);
	    
	    categoryTitleList = new JComboBox(cTL); //  Category type List
	    categoryTitleList.setBounds(170, 100, 140, 25);
		backGroundPanel.add(categoryTitleList);
		
		
		JLabel categoryItem = new JLabel("Category Item :");   // Category Item Label
		categoryItem.setBounds(50, 150, 110, 25);
		categoryItem.setFont(new Font("Serif",Font.BOLD,15));
		backGroundPanel.add(categoryItem);
		
		
		  categoryItemForCT = new JTextArea();                // Text Area .
		//categoryItemForCT.setBounds(170, 170, 140, 140);
		  JScrollPane scrollPane = new JScrollPane(categoryItemForCT); // Add Scroll bar to Text area
		  scrollPane.setBounds(170, 153,140,150);
		  scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		  backGroundPanel.add(scrollPane);
		
		
		
		// ***************************** Go back Button *******************
		
		   Action goBackAction = new AbstractAction("Go back") {

				public void actionPerformed(ActionEvent arg0) {
				
					showEventInfo.this.dispose();
					selectEventpPass.showWindow();
				}
			};
	
	
	 goBack = new JButton(goBackAction);
	 goBack.setBounds(208, 325, 100, 25);
	 goBack.addActionListener(this);
	 backGroundPanel.add( goBack);
		   
		   
			 // *********** JFrame option ************
		    setUndecorated(false);  
		    setLocationRelativeTo(null);
		    setResizable(false);
		    setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);	
		    setVisible(true);
		}
	

	public static void main(String[] args) {
	
		//new showEventInfo();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		

		
	if(e.getSource() == categoryTitleList)
	{
		selectedTitle = categoryTitleList.getSelectedItem().toString();
		selectedItem = callDB.categoryItemForSelectedCT(eventLabel, selectedTitle);
		
		String x = " " ;
		
		System.out.println(selectedItem.elementAt(0));
	
		for(int i= 0 ; i< cTL.size(); i++)
		{
//			x+= selectedItem.elementAt(i);
			categoryItemForCT.setText(selectedItem.elementAt(i)+"\n");
			System.out.println( selectedItem.elementAt(i));
			
		}
		
//		categoryItemForCT.setText(x);
		
           }
		

	    }

		
	}
	
	



