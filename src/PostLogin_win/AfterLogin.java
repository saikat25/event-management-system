package PostLogin_win;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import sun.rmi.runtime.Log;
import CreateNewEvent.addCategoryTitle;
import Login_win.BackGround;
import Login_win.LoginWindow;
import Login_win.Login_window_db;
import SelectedEvent.SelectEventType;
import SelectedEvent.createNewEvent;
import SelectedEvent.selectedEvent;

public class AfterLogin extends JDialog  implements ActionListener{
	
//	JPanel selectionPanel = new JPanel();
	
	Login_window_db callDB =new Login_window_db();
	LoginWindow afterWindow;
	addCategoryTitle addCT;
	JButton  goBack,newevent,select,delete;
	Vector<String> eL = new Vector<String>();
	 JComboBox<String> eventSelecList;
	 String selectedItem;

	
	public AfterLogin(SelectEventType afterSelectEventType)
	{
		
		
		super(afterSelectEventType, ModalityType.APPLICATION_MODAL);
		setTitle("Event Selection ");
		setSize(600, 500);
		afterSelectEventType.hideWindow();
		
		  //***************** JPanel option *****************
		
	    ImageIcon backGroundImage = new ImageIcon(LoginWindow.class.getResource("/background/8.jpg"));
		BackGround backGroundPanel = new BackGround(backGroundImage); // adding background
	    backGroundPanel.setLayout(null);
        add(backGroundPanel);
		
	 //  selectionPanel.setLayout(null);
	 //  selectionPanel.setBackground(Color.CYAN);
	  // add(selectionPanel);
	   
	   
	   //********************* JLabel option **************
	   
	   JLabel eventSelection = new JLabel("Event selection");
	   eventSelection.setFont(new Font("Serif", Font.BOLD, 20));
	   eventSelection.setBounds(50, 50, 130, 30);
	   backGroundPanel.add(eventSelection);
	   
	   // ******************** JCombo box (event Selection List)**************
	   
	   eL = callDB.addEventToSelectList();              // add event to Select List
	   
	   eventSelecList = new JComboBox<String>(eL);
	   eventSelecList.setBounds(50, 100, 130, 25);
	   backGroundPanel.add(eventSelecList);
	   eventSelecList.setSelectedItem(null);
	   
	   //************* Button (Log out)**********
	   
	   Action goBackAction = new AbstractAction("Go back") {

		public void actionPerformed(ActionEvent arg0) {
		
			AfterLogin.this.dispose();
			afterSelectEventType.showWindow();
			
			
		}
	};
	   
	   goBack = new JButton( goBackAction);
	   goBack.setBounds(480, 65, 80, 27);
	  // goBack.addActionListener(this);
	   backGroundPanel.add( goBack);
	   
	   // ************** Button (Create New Event)*************
	   
//	   
//	   Action neweventAction = new AbstractAction("Create New Event") {
//
//			public void actionPerformed(ActionEvent arg0) {
//			
//				Object addCTg;
//				new createNewEvent(AfterLogin.this,afterWindow);
//			
//				
//			}
//		};
		   
		   newevent = new JButton("Create New Event");
		   newevent.setBounds(420, 115, 140, 27);
		   newevent.addActionListener(this);
		   backGroundPanel.add(newevent);
		   
		   
		   // *************************  Button (select) *************
		   
//		   Action selectAction = new AbstractAction("Select") {
//
//			public void actionPerformed(ActionEvent e) {
//					
//				new selectedEvent(AfterLogin.this);
//				
//			}
//		};
		   
		   
		   select = new JButton("Select");
		   select.setBounds(50, 350, 70, 25);
		   select.addActionListener(this);
		   backGroundPanel.add(select);
		   
		   
   // *************************  Button Delete) *************
		   
//		   Action deleteAction = new AbstractAction("Delete") {
//
//			public void actionPerformed(ActionEvent e) {
//					
//				JOptionPane.showConfirmDialog(null, "Do you want to delete the booking ? ", // confirm box
//						"Confirmation Window",JOptionPane.YES_NO_OPTION);
//				
//			}
//		};
		   
		   
		    delete = new JButton("Delete");
		   delete.setBounds(140, 350, 70, 25);
		   delete.addActionListener(this);
		  backGroundPanel.add(delete);
		
		
		 // *********** JFrame option ************
	    setUndecorated(false);  
	    setLocationRelativeTo(null);
	    setResizable(false);
	  //setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);	// running window close 
	    setVisible(true);
	}
	
	public void hideWindow() {                  // hide window 
		this.setVisible(false);
	}
	
	public void showWindow() {					// show window
		this.setVisible(true);
	}
	
	 public String selectEvent()
	 {
	    selectedItem = ( eventSelecList.getSelectedItem().toString() );
		
		return selectedItem;
	 }
	 

	public static void main(String[] args) {
		
		// new afterLogin();   //this is for JDialouge Class

	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		
		
        if (arg0.getSource() == newevent )
		{
			new createNewEvent(AfterLogin.this,afterWindow);
			
		}
		
		else if (arg0.getSource() == select)
		{
			
			 if (eventSelecList.getSelectedItem() == null)
				{
					
					JOptionPane.showMessageDialog (null, "Select a name from the list", "Error Message", JOptionPane.ERROR_MESSAGE);    
				}
		
			 else 
			 {
				 
				 new selectedEvent(AfterLogin.this);
				 
			 }
			
			
			
			
		}
		
		else if (arg0.getSource() == delete)
		{
			
			String deleteEvent = eventSelecList.getSelectedItem().toString();
	
			System.out.println(deleteEvent);
			int action =JOptionPane.showConfirmDialog(null, "Do you want to delete the event name? ", "Delete Event Name", JOptionPane.YES_NO_OPTION);
			
			callDB.deleteFixedEventName(deleteEvent,action);
			eventSelecList.removeItem(deleteEvent);
		}
			
		
	}

}
