package Login_win;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Vector;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import PostLogin_win.AfterLogin;
import SelectedEvent.SelectEventType;
import SelectedEvent.bookingNumber;

public class LoginWindow extends JFrame implements ActionListener{
	
	ImageIcon backGroundImage = new ImageIcon(LoginWindow.class.getResource("/background/8.jpg"));
	
	//JPanel logPanel = new JPanel();
	BackGround backGroundPanel = new BackGround(backGroundImage);
	
	 Login_window_db callDB = new Login_window_db();
	
	 JButton login;
	 public JTextField userField;
	 public JPasswordField passFeild;
	 JLabel alertFeild,alertusernameFeild,alertPasswordFeild;
		
	public LoginWindow()
	
	
	
	{
		
	 	super("Event Management Software");
	    setSize(500, 400);
	    
	    
	    //***************** JPanel option *****************
	
	    backGroundPanel.setLayout(null);
        add(backGroundPanel);
	
	    //************** User Option *************
	    
	    JLabel username = new JLabel("Username :");
	    username.setBounds(50, 110, 110, 30);
	    username.setFont(new Font("Serif", Font.BOLD, 17));
	   // logPanel.add(username);
	    backGroundPanel.add(username);
	    
	    userField = new JTextField();
	    userField.setBounds(150, 114, 160, 26);
	    userField.setToolTipText("Enter the Username");
	    userField.setText(null);
	 //   logPanel.add(userField);
	    backGroundPanel.add(userField);
	    
	    
	    //************* Password option **************
	    
	    JLabel password = new JLabel("Password :");
	    password.setBounds(50, 170, 110, 30);
	    password.setFont(new Font("Serif",Font.BOLD,17));
	    backGroundPanel.add(password);
	    
	    passFeild = new JPasswordField();
	    passFeild.setBounds(150, 174, 160, 26);
	    passFeild.setToolTipText("Enter the Password");
	    passFeild.setText(null);
	 //   logPanel.add(passFeild);
	    backGroundPanel.add(passFeild);
	    
	    
	    // **************** Disable Lable *****************
	    
	    JLabel contactLabel= new JLabel("Contact us : emtious@gmail.com");
	    contactLabel.setBounds(190, 340, 210, 20);
	    contactLabel.setFont(new Font("Serif", Font.ITALIC, 15));
	    contactLabel.setForeground(Color.pink);
	    contactLabel.setEnabled(false);
	  //  logPanel.add(contactLabel);
	    backGroundPanel.add(contactLabel);
	    
	    
 //*************************** Alert username Label **************************
	    
	    alertusernameFeild = new JLabel();
	    alertusernameFeild.setBounds(330, 110, 110, 30);
	    alertusernameFeild.setFont(new Font("Serif", Font.BOLD, 15));
	   // logPanel.add(username);
	    backGroundPanel.add(alertusernameFeild);
	    
	    
 //*************************** Alert password Label **************************
	    
	    alertPasswordFeild = new JLabel();
	    alertPasswordFeild.setBounds(330, 170, 110, 30);
	    alertPasswordFeild.setFont(new Font("Serif", Font.BOLD, 15));
	   // logPanel.add(username);
	    backGroundPanel.add(alertPasswordFeild);	    
	    
	    
	 //*************************** Alert Label **************************
	    
	    alertFeild = new JLabel();
	    alertFeild.setBounds(140, 210, 200, 50);
	    alertFeild.setFont(new Font("Serif", Font.BOLD, 15));
	   // logPanel.add(username);
	    backGroundPanel.add(alertFeild);
	    
	    
	    //************** JButton (Login)****************
	    

//	    Action signInAction = new AbstractAction("Sign in") {            // Button action
//			public void actionPerformed(ActionEvent arg0) {
//				
//			
//				new AfterLogin(LoginWindow.this);
//				
//		
//			}
//		};
//	    
	    
	    login = new JButton("Sign in");
	    login.setBounds(152, 270, 80, 30);
	    login.addActionListener(this);
	    backGroundPanel.add(login);
	    
	    
	    
	    //************** JButton (SingUp)****************
	    
	    Action signUpAction = new AbstractAction("Sign up") {            // Button action
			public void actionPerformed(ActionEvent arg0) {
				
			
				new SignUp(LoginWindow.this);
				
		
			}
		};
	    
	    JButton signUp = new JButton(signUpAction);
	    signUp.setBounds(380, 270, 80, 30);
	   // logPanel.add(signUp);
	    
	    backGroundPanel.add(signUp);
	    

	    
	 
	    
	    

	    
	   // *********** JFrame option ************
	    setUndecorated(false);  
	    setLocationRelativeTo(null);
	    setResizable(false);
	//   setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);	
	    setVisible(true);
		
	
	}
	
	
	public void hideWindow() {
		
		this.setVisible(false);
		
	}
	
	public void showWindow() {
		
		this.setVisible(true);
		
	}


	public static void main(String[] args) throws InstantiationException, IllegalAccessException, UnsupportedLookAndFeelException {
		
		//***************** NIMBUS EFFECT *************(fOR GOOD INTERFACE )
		
		try {

			UIManager.setLookAndFeel( "com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
			}
		
		catch (ClassNotFoundException ex) {
		
		    }
		

	 new LoginWindow();
	 
	 
	}


	@Override
	public void actionPerformed(ActionEvent event) {
		
		boolean checkResult;
		String user,pass;
		user=userField.getText();
		pass=new String( passFeild.getPassword());
		
		if(event.getSource() == login)
		{
			
		//	if(userField.getText()== null && passFeild.getPassword(== null)
			
			
	         if(checkResult=callDB.loginCheck(user,pass))
					{
						
					//	new AfterLogin(LoginWindow.this);
						new SelectEventType(this);
						alertFeild.setText(null);
					}
				
					
			else
					{
					 
						
						//JOptionPane.showMessageDialog (null, "Enter username & password", "Error Message", JOptionPane.ERROR_MESSAGE);    
						alertFeild.setText("Worng Usernmae or Password");

						userField.setText(null);
						passFeild.setText(null);
					}
				 
			
			 
			 
			 }
			
			
			
		}
		
			
	}





