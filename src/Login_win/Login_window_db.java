package Login_win;

import java.awt.geom.RoundRectangle2D.Double;
import java.sql.*;
import java.util.Iterator;
import java.util.Stack;
import java.util.Vector;

import javax.swing.JOptionPane;

import com.sun.xml.internal.ws.api.ha.StickyFeature;

import CreateNewEvent.selectEvent_showinfo;
import CreateNewEvent.showEventInfo;
import SelectedEvent.createNewEvent;


public class Login_window_db 
{
	private String Username="root",Password="root";
	private String ConnectionString = "jdbc:mysql://localhost:3306/eventmanagementsystem";
	private Connection	MyConnection;
	private Statement EStatement;
	private PreparedStatement pS;
	private ResultSet EResultSet;
	
	//////////// Another Class///////////////////////////////
	
	selectEvent_showinfo selectedEvent;
	showEventInfo selectedCategoryTitle;
	
	
	private void createConnection() throws SQLException
	{
	
			 MyConnection = DriverManager.getConnection(ConnectionString,Username,Password);
			 
			System.out.println("Batabase connected");
		 
	}
	private void closeConnection() throws SQLException
	{
		if(MyConnection!=null)
		{
			MyConnection.close();
		}
		
	}
	


 //***************************** Input UserName And Password For new Account**********************
 
//	public void insertData (String s1, String s2, String s3 )
		public void insertData (Vector<String> Data )
	
	{
		String insert;
		
		insert= "Not inserted";
		
		try 
		{
			
			createConnection();
			
			
			EStatement = MyConnection.createStatement();
			String sql= "insert into createuser (userName,userEmail,password) values ('"+Data.elementAt(0)+"','"+Data.elementAt(1)+"','"+Data.elementAt(2)+"') ";
		    EStatement.executeUpdate(sql);
		    insert= "inserted";
		    
		   closeConnection();
			
		
			
		} 
		catch (SQLException e) 
		{
			System.err.println("Error Found in Try Block of matchField() Function");
		}
		
	}
		
		
		// ********************  Match userName and password From Database ....************************

	    public boolean loginCheck(String username, String password){
	        String query;
	        String dbUsername, dbPassword;
           boolean login = false;
            
            try {
          
				            	createConnection();
				    			EStatement = MyConnection.createStatement();
					            query = "SELECT userName, password FROM createuser where userName= '" + username + "'"; 
				                ResultSet rs =EStatement.executeQuery(query);
				                
				               
				
				            while(rs.next()){
				                dbUsername = rs.getString("userName");
				                dbPassword = rs.getString("password");

				                if(dbUsername.equals(username)&& dbPassword.equals(password)){
					                  System.out.println("OK");
				                    login = true;
				                }
				                
				                System.out.println("Username : " +dbUsername+  "Password :" +dbPassword);
				                
				               
					               
				            }
				            
				            
				            closeConnection();
				            
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
            
        return login;
        
        
	        }
		
		
	//***************************** Taking Input For The New Event From the User **************
		
	public boolean createNewEventInput(Vector<String> newEventInput )
	{
		
	//	String insert;
	
 //		insert= "Not inserted";
		boolean returnValue = false;
		
		try 
		{
			
			createConnection();
			
			
			EStatement = MyConnection.createStatement();
			String sql= "insert into createnewevent (eventName,categoryTitle,categoryItem) values ('"+newEventInput.elementAt(0)+"','"+newEventInput.elementAt(1)+"','"+newEventInput.elementAt(2)+"') ";
			
		    EStatement.executeUpdate(sql);
		    
		 //   insert= "inserted";
		    
		  returnValue= true;
		    
		    closeConnection();
			
		
			
		} 
		catch (SQLException e) 
		{
			System.err.println("Error Found in Try Block of matchField() Function");
		}
		

		return returnValue;
		
	}
	
	
//*********************************  Add event in the selectionlist	*********************
	
	
	public Vector<String> addEventToSelectList() {
		
		String query ;
		Vector<String> eventList = new Vector<String>(); 
		
		try {
			
			createConnection();
			EStatement = MyConnection.createStatement();
            query = "SELECT DISTINCT eventName FROM createnewevent "; 
            ResultSet rs =EStatement.executeQuery(query);
            
            while (rs.next()) {
            	
            	 eventList.add(rs.getString("eventName")) ;
            	
			}
            
            closeConnection();
         //   System.out.println("Event name : "+ eventList);
            
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		return eventList;
		
		
	}
	
	
	//*********************************  Add categoryTitle in the selectionlist	*********************
	
	
		public Vector<String> addCategoryToSelectList() {
			
			String query ;
			Vector<String> categoryList = new Vector<String>(); 
			
			System.out.println(selectedEvent);
			try {
				
				createConnection();
				EStatement = MyConnection.createStatement();
	            query = "SELECT  categoryTitle FROM createnewevent "; 
	            ResultSet rs =EStatement.executeQuery(query);
	            
	            while (rs.next()) {
	            	
	            	categoryList.add(rs.getString("categoryTitle")) ;
	            	
				}
	            
	            closeConnection();
	         //   System.out.println("Categoryname name : "+ categoryList);
	            
			} catch (Exception e) {
				// TODO: handle exception
			}
			
			return categoryList;
			
			
		}
	
	
	//********************************* ADD New Category Title To An Event *****************
	
	
	public boolean addNewCategoryTitle( Vector<String> newCategory) {
		
		boolean dbResponse = false;
		
	   System.out.println("Eventname " +newCategory.elementAt(0)+"Cate "+newCategory.elementAt(1));
		
		try {
			
			createConnection();
			
			
			EStatement = MyConnection.createStatement();
			String sql= "insert into createnewevent (eventName,categoryTitle,categoryItem) values ('"+newCategory.elementAt(0)+"','"+newCategory.elementAt(1)+"','"+" "+"') ";
			
		    EStatement.executeUpdate(sql);
		    dbResponse = true;
		    
			System.out.println("Inserted");
			
			closeConnection();
			
 
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		return dbResponse ;
		
	}
	
	
	//********************************* ADD New Category Item To An Event *****************
	
	
		public boolean addNewCategoryItem( Vector<String> newCategory) {
			
			boolean dbResponse = false;
			String query;	
			System.out.println("Eventname " +newCategory.elementAt(0)+"Cate "+newCategory.elementAt(1));
			
			try {
				
	createConnection();
				
				
				EStatement = MyConnection.createStatement();
				String sql= "insert into createnewevent (eventName,categoryTitle,categoryItem) values ('"+newCategory.elementAt(0)+"','"+newCategory.elementAt(1)+"','"+newCategory.elementAt(2)+"') ";
				
			    EStatement.executeUpdate(sql);
			    
			    dbResponse = true;
			    
				System.out.println("Inserted");
			
				closeConnection();
	 
			} catch (Exception e) {
				// TODO: handle exception
			}
			
			return dbResponse ;
			
		}
		
		
		// *************************************  CategoryTitle For an event For showing info********************
		
		public Vector<String> categoryTitleForSelectedEvent ( String selectedItem)
	
		{
			Vector<String> dbCategoryTitle = new Vector<String>();

		
			try {
				
				createConnection();
				EStatement = MyConnection.createStatement();				
				String sql = "SELECT categoryTitle FROM createnewevent where eventName = '"+selectedItem+"'";
				EResultSet = EStatement.executeQuery(sql);
				
				while(EResultSet.next())
				{
				
					dbCategoryTitle.add(EResultSet.getString("categoryTitle"));
					
					
				}

				
				closeConnection();
				
			} 
			
			catch (Exception e) {
				
			}
			
			return dbCategoryTitle;
			
			
		}
		
		
		//****************************** Show categoryItem For A categoryTitle in a List / textArea ****************** 
		
		public Vector<String> categoryItemForSelectedCT ( String selectedEvent ,String selectedItem)
		
		{
			Vector<String> dbCategoryItem  = new Vector<String>();
			// String categoryTitle = selectedCategoryTitle.selectCategory();			
			try {
				
				createConnection();
				EStatement = MyConnection.createStatement();				
				String sql = "SELECT categoryItem FROM createnewevent where eventName = '"+selectedEvent+"' AND categorytitle ='"+selectedItem+"' ";
	            EResultSet = EStatement.executeQuery(sql);
				
				while(EResultSet.next())
				{
				
					dbCategoryItem.add(EResultSet.getString("categoryItem"));
					
					
				}
				
				System.out.println("CI" + dbCategoryItem.elementAt(0));

				closeConnection();
				
			} catch (Exception e) {
				
			}
			
			return dbCategoryItem;
			
			
		}
	
		
	//************************************ Calculate Total Charge For Customize Package ******************
		
		
		
				public String calculateTotalCharge (String eventName)
				
				{
					String sum = null;
					
					System.out.println(eventName);
					try {
						
						createConnection();
						EStatement = MyConnection.createStatement();
						String sql = "SELECT sum(totalCharge) FROM createcustomizepackage where eventName = '"+eventName+"'";
						 EResultSet = EStatement.executeQuery(sql);
						 
					    if(EResultSet.next())
					    	{
					    	
					    	 sum = EResultSet.getString("sum(totalCharge)");  
					    	
					    	 System.out.println(sum);
					    	}
                      
//                      
//                          
                       
			

						closeConnection();
						
					} catch (Exception e) {
						
					}
					
				return sum;
				
					
				}
				
			
		
	
   // ********************************  Create Fixed Pakage (Name) *****************************
		
		
		public String createFiexedPakage ( Vector< String> pakageInfoList)
		{
			
	
			 try {
				 
				    createConnection();
				 
					EStatement = MyConnection.createStatement();
					String sql= "insert into createfixedpackage (packageName,categoryTitle,categoryItem,eventName,totalCharge) values ('"+pakageInfoList.elementAt(0)+"','"+pakageInfoList.elementAt(1)+"',"
							+ "'"+pakageInfoList.elementAt(2)+"','"+pakageInfoList.elementAt(3)+"','"+pakageInfoList.elementAt(4)+"' ) ";
			        EStatement.executeUpdate(sql);
			
			        System.out.println("Inserted");
				 
				 
				 
				closeConnection();
				    
			} catch (Exception e) {
				// TODO: handle exception
			}
			 
			 return pakageInfoList.elementAt(0);
	
		}
		
		
	// ******************** Fixed Package List *****************************
		
		public Vector<String> FixedPackageList (String eventName)
		
		{
			Vector< String> dbPackageList = new Vector<String>();
		
			try {
				
				createConnection();
				EStatement = MyConnection.createStatement();				
				String sql2 ="SELECT packageName FROM createfixedpackage where eventName = '"+eventName+"' ";
			    EResultSet = EStatement.executeQuery(sql2);
					
					while(EResultSet.next())
					{
					
						dbPackageList.add(EResultSet.getString("packageName"));
						
						
					}

				
			} catch (Exception e) {
				// TODO: handle exception
			}
			
			return dbPackageList;
		}

		
		// *******************************  Customize Pakage  info List ************************
		
		
		 public void createCustomizePackage ( Vector<String> customizePakageInfoList)
		 {
			 
			 try {
				
				 
				    createConnection();
					EStatement = MyConnection.createStatement();
					String sql= "insert into createcustomizepackage (eventName,categoryItem,totalCharge) values ('"+customizePakageInfoList.elementAt(0)+"','"+customizePakageInfoList.elementAt(1)+"',"
							+ "'"+customizePakageInfoList.elementAt(2)+"' ) ";
			        EStatement.executeUpdate(sql);
			
			        System.out.println("Inserted");
				 
				 
				 
				closeConnection();
		 
			} catch (Exception e) {
				// TODO: handle exception
			}
			 
			 
		 }
		 
		 
		//**************************************** Customize Package Booking Form Data ************************
		 
		   public void customizePackageBookingFormDAta( Vector<String> customizePakageBookingForm, int BN )
		   {
			   
			   String ami = "Fixed" ;
			   
			   System.out.println("Booking Number : " + BN);
			   System.out.println("Fisrt element :" + customizePakageBookingForm.elementAt(9));
			   
			   try {
				   
				    createConnection();
					EStatement = MyConnection.createStatement();
					String sql= "insert into bookingcustomizepackage (name,address,ContactNum,timeOfparty,numOfperson,totalCharge,advanceCharge,balanceAmount,packageDate,entryNum,eventName,eventType) values ('"+customizePakageBookingForm.elementAt(0)+"','"+customizePakageBookingForm.elementAt(1)+"','"+customizePakageBookingForm.elementAt(2)+"','"+customizePakageBookingForm.elementAt(3)+"','"+customizePakageBookingForm.elementAt(4)+"','"+customizePakageBookingForm.elementAt(5)+"','"+customizePakageBookingForm.elementAt(6)+"','"+customizePakageBookingForm.elementAt(7)+"','"+customizePakageBookingForm.elementAt(8)+"','"+BN+"','"+customizePakageBookingForm.elementAt(9)+"','"+ami+"')"; 
					
			         EStatement.executeUpdate(sql);
			
			        System.out.println("Element inserted");
			        JOptionPane.showConfirmDialog(null,"Booking NUmmber is "+BN,"Booking Message",JOptionPane.CLOSED_OPTION);
				 
				 
				 
				closeConnection();
				   
				   
				
			} catch (Exception e) {
				// TODO: handle exception
			}
			   
			   
		   }
		   

		   
			//****************************************Fixed Package Booking Form Data ************************
			 
			   public void fixedPackageBookingFormDAta( Vector<String> fixedPakageBookingForm , int BN )
			   {
				   
				   
				    String ami = "Fixed" ;
				   try {
					   
					   createConnection();
						EStatement = MyConnection.createStatement();
						String sql= "insert into bookingfixedpackage (name,address,ContactNum,timeOfparty,numOfperson,totalCharge,advanceCharge,balanceAmount,packageDate,entryNum,eventName,eventType) values ('"+fixedPakageBookingForm.elementAt(0)+"','"+fixedPakageBookingForm.elementAt(1)+"','"+fixedPakageBookingForm.elementAt(2)+"','"+fixedPakageBookingForm.elementAt(3)+"','"+fixedPakageBookingForm.elementAt(4)+"','"+fixedPakageBookingForm.elementAt(5)+"','"+fixedPakageBookingForm.elementAt(6)+"','"+fixedPakageBookingForm.elementAt(7)+"','"+fixedPakageBookingForm.elementAt(8)+"','"+BN+"','"+fixedPakageBookingForm.elementAt(9)+"','"+ami+"')"; 
						
				         EStatement.executeUpdate(sql);
				
				        System.out.println("Element inserted");
				        JOptionPane.showConfirmDialog(null,"Booking NUmmber is "+BN,"Booking Message",JOptionPane.CLOSED_OPTION);
					 
					 
					 
					closeConnection();
					   

				} catch (Exception e) {
					// TODO: handle exception
				}
				   
				   
			   }
			   
			   

			//****************************************Fixed Package Information ************************

             public Vector<String>  fixedPackageDetail() 
             
             {
            	 
            	 Vector<String> info = new Vector<String>();
            	 try {
            		 
            		// System.out.println(packageinformation.elementAt(0));
            		// System.out.println(packageinformation.elementAt(1));
					
            		 createConnection();
					 EStatement = MyConnection.createStatement();
					 String query = "SELECT * FROM createfixedpackage ";
					 
					 EResultSet = EStatement.executeQuery(query);
						int i=0;	
							while(EResultSet.next())
							{
								
								info.add(EResultSet.getString(i));							
								i++;	
							}
							System.out.println(info.elementAt(1));
							
							
							closeConnection();
            		 
				} catch (Exception e) {
					// TODO: handle exception
				}
            	 
				return info;
			} 
             
             
             
       //********************************************* NonFixed _ EventName _ Category Name_ Input****************************
             
             
             public void nonFixedEventnameCategoryName(Vector<String> eventName, Vector<String>  category_input)
             
             {
            	 
            	 System.out.println("EVE"+eventName.elementAt(0));
            	 
            	 try {
            		 
            		 createConnection();
					 EStatement = MyConnection.createStatement();
            		 String query = "insert into nonfixed_event_category(EventName,Category_SubcategoryName) values ('"+eventName.elementAt(0)+"','"+ category_input.elementAt(0)+"')";
 			         EStatement.executeUpdate(query);
 					
			       System.out.println("Inserted");
 
				closeConnection();
            		 
				} 
            	 
            	 
            	 catch (Exception e) {
					
					
				}
            	 
            	 
       
            	 
             }
             
             
       //******************************** Add Event List For ( sub Category & to access specific Event )in Non-fixed Event **************************
             
             
            public Vector<String> getNonFixedEventList ()
            {
            	Vector<String> EventName = new Vector<String>();
            
            	
            	try {
            		
            		 createConnection();
            		 EStatement = MyConnection.createStatement();
            		 String query = "SELECT DISTINCT (EventName) FROM nonfixed_event_category ";
            		 EResultSet = EStatement.executeQuery(query);
						
							while(EResultSet.next())
							{
								
								EventName.add(EResultSet.getString("EventName"));
							
							}
            		
            		
					
				} catch (Exception e) {
					// TODO: handle exception
				}
         
            	return EventName ;
            	
            }
            
            
            
            
            //******************************** Add Category List For sub Category in Non_Fixed Event **************************
            
            
            public Vector<String> getNonFixedCatrgoryList (String selectedEvent)
            {
            
            	Vector<String> categoryName = new Vector<String>();
            	
            	try {
            		
            		 createConnection();
            		 EStatement = MyConnection.createStatement();
            		 String query = "SELECT DISTINCT Category_SubcategoryName FROM nonfixed_event_category  where EventName = '"+selectedEvent+"' ";
            		 EResultSet = EStatement.executeQuery(query);
		
							while(EResultSet.next())
							{
	
								categoryName.add(EResultSet.getString("Category_SubcategoryName"));
							
							}
            		

				      } catch (Exception e) {
					// TODO: handle exception
				}
         
            	return categoryName ;
            	
            }   
            
            
            
            
       //********************************************* NonFixed _ EventName _ (CATEGORY_Sub_Category Name)together_ Input-(Take Place in Category List)****************************
             
             
        public void nonFixedEventname_SubCategoryName(Vector<String> eventName, Vector<String>  Subcategory_input)
             
             {
        	
        //	Vector<String> item = new Vector<String>();
            	 
            	 try {
            		 
            		 createConnection();
            		
					 EStatement = MyConnection.createStatement();
					 int i;
            		 for(i=0;i<Subcategory_input.size();i++)
            		 {
            			 
                		 String query = "insert into nonfixed_event_category(EventName,Category_SubcategoryName) values ('"+eventName.elementAt(0)+"','"+ Subcategory_input.elementAt(i)+"')";
     			         EStatement.executeUpdate(query); 
     			 
            		 }

// 				
//            		 
            		 
			       System.out.println("Inserted");
 
				closeConnection();
            		 
				} 
            	 
            	 
            	 catch (Exception e) {
					
					
				}
            	 
            	 
       
            	 
             }
        
        
        
        
        //********************************************* NonFixed _ EventName _Category_ Sub_Category Name_ Input****************************
        
        
        public void nonFixedEvent_Category_SubCategoryName(Vector<String> eventName, Vector<String>  Subcategory_input)
             
             {
        	
           	 
            	 try {
            		 
            		 createConnection();
            		
					 EStatement = MyConnection.createStatement();
					 int i;
            		 for(i=0;i<Subcategory_input.size();i++)
            		 {
            			 
                		 String query = "insert into nonfixed_category_subcategory(EventName,CategoryName,SubcategoryName) values ('"+eventName.elementAt(0)+"','"+eventName.elementAt(1)+"','"+ Subcategory_input.elementAt(i)+"')";
     			         EStatement.executeUpdate(query); 
     			         
     			    
            		 }
            			 
			       System.out.println("Inserted");
 
				closeConnection();
            		 
				} 
            	 
            	 
            	 catch (Exception e) {
					
					
				}
            	 
            	 
       
            	 
             }
        
        
        
        
        //********************************************* NonFixed _Create_New Form_****************************
        
        
        public void nonFixedEvent_CreateNewForm(String selectedEvent,int formNum ,String formName, Vector<String>  formInfo)
             
             {
        	
        	Vector<String> item = new Vector<String>();
       
            	 
            	 try {
            		 
            		 createConnection();
            		
					 EStatement = MyConnection.createStatement();
					 int i;
            		 for(i=0;i<formInfo.size();i++)
            		 {
            			 
                		 String query = "insert into nonfixed_createform_info(EventName,FormNum,FormName,InfoTitle) values ('"+selectedEvent+"','"+formNum+"','"+formName+"','"+ formInfo.elementAt(i)+"')";
     			         EStatement.executeUpdate(query); 
     			         
     			      item.add(query); 
            			 
     			       // nonfixed_category_subcategory
            		 }

 					int j ;
 					for (j=0;j<item.size();j++)
 					{
 						
 						System.out.println(item.elementAt(j));
 						
 					}
            		 
            		 
			       System.out.println("Inserted");
 
				closeConnection();
            		 
				} 
            	 
            	 
            	 catch (Exception e) {
					
					
				}
            	 
            	 
       
            	 
             }
        

        //************************** Add Form Name to List for fill up the form *****************
        
        public  Vector<String> addFormName()
        
        {
        	Vector<String> formName = new Vector<String>();
        	
        	try {
        		
        		 createConnection();
        		 EStatement = MyConnection.createStatement();
        		 String query = "SELECT DISTINCT FormName FROM nonfixed_createform_info ";
        		 EResultSet = EStatement.executeQuery(query);
	
						while(EResultSet.next())
						{

							formName.add(EResultSet.getString("FormName"));
						
						}
        		
				
			} catch (Exception e) {
				// TODO: handle exception
			}
        	
        	
        	return formName;
        }
        
//        
//        //******************************* Show Non_fixedEvent Information***************************
//        
//        
//        public showNonfixedEventInfo()
//        {
//        	
//        	try {
//
//        		createConnection();
//        		
//       		 EStatement = MyConnection.createStatement();
//       		 String query = "SELECT DISTINCT FormName FROM nonfixed_createform_info ";
//       		 EResultSet = EStatement.executeQuery(query);
//	
//						while(EResultSet.next())
//						{
//
//							formName.add(EResultSet.getString("FormName"));
//						
//						}
//        		
//        		
//        		
//        		closeConnection();
//        		
//        		
//        		
//			} catch (Exception e) {
//				// TODO: handle exception
//			}
//        	
//        	
//        	
//        	
//        	
//        	
//        }
        
        
        //********************************Delete Event Name From the List (Fixed Event)*************************
        
        
        public void deleteFixedEventName(String eventname, int action)
        {
        	
        
        	System.out.println(eventname);
        	
        	System.out.println(action);
        	if(action == 0)
        	{
        		try {
    				
            		createConnection();
            		 EStatement = MyConnection.createStatement();
    				 
                     String query = "delete from createnewevent where eventName = '"+eventname+"' ";
     			     EStatement.executeUpdate(query); 
     			    JOptionPane.showMessageDialog(null, "Deleted successfully Update");
     			     
            		 }

            		
    			 catch (Exception e) {

    				  JOptionPane.showMessageDialog( null, e.getMessage(), "Database Messages",
    	                      JOptionPane.ERROR_MESSAGE );
    				 
    			}
            	
        		
        		
        	}
        	
        	
        		
        	
			
            }
        
    //*************************************** ADD Booking Number (Customize Booking -------- >>>> Fixed Event)*************************    
        
        public void addbookingNumber (Vector<String> eventname, int bN)
        {

        	
       	String ami = "tumi" ;

        	try {
        		
        		
       		 createConnection();
   		     EStatement = MyConnection.createStatement();
   		     
             String sql = "insert into bookingdata(packageDate,entryNum,eventName,eventCategory) values('"+eventname.elementAt(0)+"','"+bN+"','"+eventname.elementAt(1)+"','"+ami+"')"; 
		     EStatement.executeUpdate(sql);
			
    		System.out.println("Inserted booking Number ");
    	 
    	    JOptionPane.showConfirmDialog(null,"Booking NUmmber is "+bN,"Booking Message",JOptionPane.CLOSED_OPTION);
				
			} catch (Exception e) {
				
				System.err.println(e.getMessage());

			}
        	

        	
           }

        
        
        //*********************************Search Booking Number ****************************
        
        public int searchBookingNumber()
        {
        	int bN = 0 ;
        	Vector<String> endnumber = new Vector<String>();
        	
        	try {
        		
       		 createConnection();
       		 EStatement = MyConnection.createStatement();
       		 String query = "SELECT entryNum FROM bookingdata ";
       		 EResultSet = EStatement.executeQuery(query);
	
						while(EResultSet.next())
						{

							endnumber.add(EResultSet.getString("entryNum"));
						    bN++;

						}
        		
        		System.out.println(bN);
				
			} catch (Exception e) {
				// TODO: handle exception
			}
	
        	return bN;
        }
        
   //***************************************Delete Booking Number*********************
        
       public void deleteBookingNumber(String bN , int action)
       {

       	if(action == 0)
       	{
       		try {
   				
           		createConnection();
           		 EStatement = MyConnection.createStatement();
   				 
                    String query = "delete FROM bookingdata where entryNum = '"+bN+"' ";
    			     EStatement.executeUpdate(query); 
    			    JOptionPane.showMessageDialog(null, "Deleted successfully Update");
    			     
           		 }

           		
   			 catch (Exception e) {

   				  JOptionPane.showMessageDialog( null, e.getMessage(), "Database Messages",
   	                      JOptionPane.ERROR_MESSAGE );
   				 
   			}
           	
       		
       		
       	}

    
       }
       
   //***************************** ADD Booking Number in the list*************  
       
      public Vector<String> bookingNumberList()
      {
    	   
    	  Vector<String> addtoList = new Vector<String>();
    	  try {
			
    			 createConnection();
           		 EStatement = MyConnection.createStatement();
           		 String query = "SELECT entryNum FROM bookingdata ";
           		 EResultSet = EStatement.executeQuery(query);
    	
    						while(EResultSet.next())
    						{

    							addtoList.add(EResultSet.getString("entryNum"));

    						}
    		  
		} catch (Exception e) {
			// TODO: handle exception
		}
    	  
    	  return addtoList;
      }
      
      
      
      
      //***************************** ADD Booking Number(Fixed) in the list*************  
      
      public Vector<String> fixedbookingNumberList()
      {
    	   
    	  Vector<String> addtoList = new Vector<String>();
    	  try {
			
    			 createConnection();
           		 EStatement = MyConnection.createStatement();
           		 String query = "SELECT entryNum FROM bookingfixedpackage ";
           		 EResultSet = EStatement.executeQuery(query);
    	
    						while(EResultSet.next())
    						{

    							addtoList.add(EResultSet.getString("entryNum"));

    						}
    		  
		} catch (Exception e) {
			// TODO: handle exception
		}
    	  
    	  return addtoList;
      }
      
      
      
      
      
      //***************************** ADD Booking Number(Customize) in the list*************  
      
      public Vector<String> customizebookingNumberList()
      {
    	   
    	  System.out.println("Adding List to customize List;");
    	  
    	  Vector<String> List = new Vector<String>();
    	  try {
			
    			 createConnection();
           		 EStatement = MyConnection.createStatement();
           		 String query = "SELECT entryNum FROM bookingcustomizepackage";
           		 EResultSet = EStatement.executeQuery(query);
    	
    						while(EResultSet.next())
    						{

    							List.add(EResultSet.getString("entryNum"));
    							System.out.println(List.elementAt(0));

    						}
    		  
		} catch (Exception e) {
			// TODO: handle exception
		}
    	  
    	  return List;
      }
      
      //************************************* Check Booking Number For selecting Fixed Event Type*******************
      
      
      
      public int checkBookingNumber( int sBn)
      {
 
    	  System.out.println("Ckeck Number " + sBn);
    	  
    	  int  status =0 ;
    	  String a = String.valueOf(status);
    	  try {
			
    			 createConnection();
           		 EStatement = MyConnection.createStatement();
           		 String query = "SELECT entryNum FROM bookingcustomizepackage ";
           		 EResultSet = EStatement.executeQuery(query);
           		 
           	
    	
    						while(EResultSet.next())
    						{

    						//	System.out.println(Integer.parseInt(EResultSet.getString("entryNum")));
    							
    							if(a.equals(EResultSet.getString("entryNum"))) 
    							{
    								status = 1;
        							
        							System.out.println("Matched");
    								
    							}
    

    						}
    						
    						
    						 String sql = "SELECT entryNum FROM bookingfixedpackage ";
    						 ResultSet EResultSet1 = EStatement.executeQuery(sql);
    		           		 
    		           		 while(EResultSet1.next())
    		           		 {
    		           			 
    		           			 
    		           			if(a.equals(EResultSet1.getString("entryNum"))) 
    							{
    								status = 0;
        							
        							System.out.println("UNMatched");
    								
    							}
    		           			 
    		           			 
    		           		 }
    		  
		} catch (Exception e) {
			// TODO: handle exception
		}
    	  
    	  return status;
      }
      
      
      
      //****************************** Show Booking info Of Customize Package Event into A Teaxtarea ****************************
      
      public Vector<String> showCustomizePackageBookingInfo(int entryNum)
      {
    	  Vector<String> carryInfoToshow = new Vector<String>();
    	  
    	  
    	  try {
    		  
 			 createConnection();
       		 EStatement = MyConnection.createStatement();
       		 String query = "SELECT * FROM bookingcustomizepackage where entryNum = '"+entryNum+"'";
       		 EResultSet = EStatement.executeQuery(query);

                     	while(EResultSet.next())
						{

                     		
                     	
							carryInfoToshow.add(EResultSet.getString("name")+"\n\n");
							carryInfoToshow.add(EResultSet.getString("address")+"\n\n");
							carryInfoToshow.add(EResultSet.getString("ContactNum")+"\n\n");
							carryInfoToshow.add(EResultSet.getString("timeOfparty")+"\n\n");
							carryInfoToshow.add(EResultSet.getString("numOfperson")+"\n\n");
							carryInfoToshow.add(EResultSet.getString("totalCharge")+"\n\n");
							carryInfoToshow.add(EResultSet.getString("advanceCharge")+"\n\n");
							carryInfoToshow.add(EResultSet.getString("balanceAmount")+"\n\n");
							carryInfoToshow.add(EResultSet.getString("packageDate")+"\n\n");
							carryInfoToshow.add(EResultSet.getString("entryNum")+"\n\n");
							carryInfoToshow.add(EResultSet.getString("eventName")+"\n\n");
							carryInfoToshow.add(EResultSet.getString("eventType")+"\n\n");
					
						} 
                     	
                    	System.out.println("value adding"+"\n");
                     //	System.out.println(carryInfoToshow.elementAt(0));
    		  
			
		} catch (Exception e) {
			// TODO: handle exception
		}
    	  
    	  
		return carryInfoToshow;
    	  
    	  
    	  
    	  
      }
      
      
      
      //****************************** Show Booking info Of Fixed Package Event into A Teaxtarea ****************************
      
      public Vector<String> showFixedPackageBookingInfo(int entryNum)
      {
    	  Vector<String> carryInfoToshow = new Vector<String>();
    	  
    	  
    	  try {
    		  
 			 createConnection();
       		 EStatement = MyConnection.createStatement();
       		 String query = "SELECT * FROM bookingfixedpackage where entryNum = '"+entryNum+"'";
       		 EResultSet = EStatement.executeQuery(query);

                     	while(EResultSet.next())
						{

                     		
                     	
							carryInfoToshow.add(EResultSet.getString("name")+"\n\n");
							carryInfoToshow.add(EResultSet.getString("address")+"\n\n");
							carryInfoToshow.add(EResultSet.getString("ContactNum")+"\n\n");
							carryInfoToshow.add(EResultSet.getString("timeOfparty")+"\n\n");
							carryInfoToshow.add(EResultSet.getString("numOfperson")+"\n\n");
							carryInfoToshow.add(EResultSet.getString("totalCharge")+"\n\n");
							carryInfoToshow.add(EResultSet.getString("advanceCharge")+"\n\n");
							carryInfoToshow.add(EResultSet.getString("balanceAmount")+"\n\n");
							carryInfoToshow.add(EResultSet.getString("packageDate")+"\n\n");
							carryInfoToshow.add(EResultSet.getString("entryNum")+"\n\n");
							carryInfoToshow.add(EResultSet.getString("eventName")+"\n\n");
							carryInfoToshow.add(EResultSet.getString("eventType")+"\n\n");
					
						} 
                     	
                    	System.out.println("value adding"+"\n");
                     //	System.out.println(carryInfoToshow.elementAt(0));
    		  
			
		} catch (Exception e) {
			// TODO: handle exception
		}
    	  
    	  
		return carryInfoToshow;
    	  
  
      }
      
      
      // ******************** Take form title into a Vector **************************
      
      public  Vector<String> addformTitle()
      {

    	  Vector<String> title = new Vector<String>();
    	  
    	  try {
			
    		  createConnection();
     		 EStatement = MyConnection.createStatement();
     		 String query = "SELECT entryNum FROM bookingcustomizepackage ";
     		 EResultSet = EStatement.executeQuery(query);
 
 	
 			while(EResultSet.next())
 				{

 		

 				}  
	  
		} catch (Exception e) {
			// TODO: handle exception
		}
		return null;
    	  
    	  
    	  
    	  
    	  
    	  
    	  
    	  
    	  
    	  
    	  
    	  
      }
        
          
}


        	

            

