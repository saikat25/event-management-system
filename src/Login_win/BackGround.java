package Login_win;

import java.awt.Graphics;

import javax.swing.ImageIcon;
import javax.swing.JPanel;

public class BackGround extends JPanel{
	
	ImageIcon icon;

	public BackGround(ImageIcon backGroundImage) {
		this.icon = backGroundImage;
	}
	
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.drawImage(icon.getImage(), 0, 0, this.getSize().width,
				this.getSize().height, this);
	}

}
