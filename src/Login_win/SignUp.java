package Login_win;

import java.awt.Color;
import java.awt.Dialog.ModalityType;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Calendar;
import java.util.Vector;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import PostLogin_win.AfterLogin;

public class SignUp extends JDialog implements ActionListener {
	
	
	JPanel backGroundPanel = new JPanel();
	
	Login_window_db insetDB = new Login_window_db();
	JButton register;
	JTextField userField;
	 JTextField emailField;
	 JPasswordField passFeild;
	 JLabel createComplete;
	

	
	public SignUp(LoginWindow window)
	{
	
		super(window,ModalityType.MODELESS) ;
		setTitle("New User");
		setSize(450, 650);
		window.hideWindow();
		
		
	    
	    //***************** JPanel option *****************
		
		 ImageIcon backGroundImage = new ImageIcon(LoginWindow.class.getResource("/background/8.jpg"));
			BackGround backGroundPanel = new BackGround(backGroundImage); // adding background
		    backGroundPanel.setLayout(null);
	        add(backGroundPanel);
		
		
	   // signUpPanel.setLayout(null);
	  //  signUpPanel.setBackground(Color.CYAN);
	   // add(signUpPanel);
	    
	    
	    //************** JLabel  *************
	    
	    
	    JLabel coverLabel = new JLabel("Create new user");  // coverLabel Label
	    coverLabel.setBounds(150, 22, 180, 30);
	    coverLabel.setFont(new Font("Serif", Font.BOLD, 20));
	    backGroundPanel.add(coverLabel);
	    

	    JLabel username = new JLabel("User name :");  // User name Label
	    username.setBounds(50, 80, 80, 30);
	    username.setFont(new Font("Serif", Font.BOLD, 15));
	    backGroundPanel.add(username);
	    
	    userField = new JTextField();      // User name Field
	    userField.setBounds(150, 84, 150, 25);
	    userField.setToolTipText("Enter the Username");
	    backGroundPanel.add(userField);
	    
	    
	    JLabel email = new JLabel("Email :");  // Email Label
	    email.setBounds(50, 135, 80, 30);
	    email.setFont(new Font("Serif", Font.BOLD, 15));
	    backGroundPanel.add(email);
	    
	    emailField = new JTextField();      // Email Field
	    emailField.setBounds(150, 139, 150, 25);
	    emailField.setToolTipText("Enter the email");
	    backGroundPanel.add(emailField);
	    
	    
	    JLabel pasword = new JLabel("Password :");  // Password Label
	    pasword.setBounds(50, 190, 150, 25);
	    pasword.setFont(new Font("Serif", Font.BOLD, 15));
	    backGroundPanel.add(pasword);
	    
	    passFeild = new JPasswordField();  // Password Field Label
	    passFeild.setBounds(150, 194, 150, 25);
	    passFeild.setToolTipText("Enter the Password");
	    backGroundPanel.add(passFeild);
	    
	//*************************** Create Complete Lable********************
	    
	    createComplete = new JLabel();  // Password Label
	    createComplete.setBounds(150, 220, 100, 25);
	    createComplete.setFont(new Font("Serif", Font.BOLD, 15));
	    backGroundPanel.add(createComplete);
	    
	    
	    
	    
	//    ************** JButton ( Register )****************
	    
//    Action registerAction = new AbstractAction("Register") {            // Button action
//		
//	public void actionPerformed(ActionEvent arg0) {
//		
//	
//		}
//		
//    };
	    
	    register = new JButton("Register");
	    register.setBounds(150, 254, 90, 25);
	    register.setFont(new Font("Serif", Font.BOLD, 13));
	    register.addActionListener(this);
	    backGroundPanel.add(register);
	    
	    
	    
	    //************** JButton ( Go back )****************
	    
	    Action goBackAction = new AbstractAction("Go back") {            // Button action
			public void actionPerformed(ActionEvent arg0) {
				
				SignUp.this.dispose();
				createComplete.setText(null);
				window.showWindow();
			}
		};
	    
	    JButton goBack = new JButton(goBackAction);
	    goBack.setBounds(330, 254, 90, 25);
	    goBack.setFont(new Font("Serif", Font.BOLD, 13));
	    backGroundPanel.add(goBack);
	    
	    
	  
	    
	    
	    
	    // create a sql date object so we can use it in our INSERT statement
	      Calendar calendar = Calendar.getInstance();
	      java.sql.Date startDate = new java.sql.Date(calendar.getTime().getTime());
	    
		
		
		
		   // *********** JFrame option ************
	    setUndecorated(false);  
	    setLocationRelativeTo(null);
	    setResizable(false);
	    setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);	
	    setVisible(true);
		
		
		
	}
	



	public static void main(String[] args) {

		//new SignUp();

	}




	@Override
	public void actionPerformed(ActionEvent arg0) {        // insert Action Listener

		Vector<String>Data = new Vector<String>();
		
		//String user = userField.getText(),email= emailField.getText(), pass= new String(passFeild.getPassword());
		
		Data.add(userField.getText());
		Data.add(emailField.getText());
		Data.add(new String(passFeild.getPassword()));
		if(arg0.getSource() == this.register)
	
		{
			
		// insetDB.insertData(user, email, pass);
		
		insetDB.insertData(Data);
		
	//	createComplete.setText("User created");
		
		JOptionPane.showMessageDialog(null, "New User Created", "Confirmation Message",JOptionPane.INFORMATION_MESSAGE);
			
		 
		 userField.setText (null);
         emailField.setText(null); //review  
         passFeild.setText (null);
	
			
		}
		
	}

}
