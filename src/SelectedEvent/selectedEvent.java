package SelectedEvent;

import java.awt.Color;
import java.awt.Dialog.ModalityType;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import pakageBookingForm.fixedPakageBooking;
import CreateNewEvent.addCategoryTitle;
import EventPakageCreate.createFixedPakage;
import EventPakageCreate.customizePakageDate;
import EventPakageCreate.fixedPakageDate;
import Login_win.BackGround;
import Login_win.LoginWindow;
import Login_win.Login_window_db;
import PostLogin_win.AfterLogin;

public class selectedEvent extends JDialog implements ActionListener {
	
	//JPanel  backGroundPanel = new JPanel();
	
	Login_window_db callDB = new Login_window_db();
	String eventLabel,newFixedPAckage,selectedPackage;
	JButton createFixedPakage,createCustomizePakage,checkBookingInfo,goBack,selectPakage;
	JMenu logOutMenu;
	LoginWindow afteLoginWindow;
	Vector<String> FixedPackageList = new Vector<String>(); 
    JLabel eventName;
    public JComboBox pakageList;
    AfterLogin pass;
    
	public selectedEvent(AfterLogin selectedEvent)
	{
		
		super(selectedEvent, ModalityType.APPLICATION_MODAL);
		setTitle("Selected Event");
		setSize(600, 500);
		selectedEvent.hideWindow();
		pass = selectedEvent;
		  //***************** JPanel option *****************
		
		   ImageIcon backGroundImage = new ImageIcon(LoginWindow.class.getResource("/background/8.jpg"));
			BackGround backGroundPanel = new BackGround(backGroundImage); // adding background
		    backGroundPanel.setLayout(null);
	        add(backGroundPanel);
	
		 // ***************** JMenubar option *********************
		   
		   	JMenuBar menuBar= new JMenuBar();
            setJMenuBar(menuBar); // Add the menu bar to the window
		
		    
		    JMenu aboutMenu = new JMenu("About"); // Create About menu
		 //   JMenuItem newMenuItem = new JMenuItem("New");
		 //   fileMenu.add(newMenuItem);

		  //  fileMenu.addSeparator();

		  //  JMenuItem saveMenuItem = new JMenuItem("Save");
		   // fileMenu.add(saveMenuItem);
		    
//		    
//		    Action logoutAction = new AbstractAction("Log out") {
//
//				public void actionPerformed(ActionEvent arg0) {
//				
//					
//					new LoginWindow();
//					
//					
//				}
//			}; 
		    
		    logOutMenu = new JMenu("Log out"); // Create log out menu
		    menuBar.add(aboutMenu); // Add the about menu
		    menuBar.add(logOutMenu); // Add the logout menu
		    
		    
		    //*************** JLabel option *******************
		    
		    eventLabel = selectedEvent.selectEvent();
		    
		    eventName = new JLabel(eventLabel);   // Event Name Label
			eventName.setBounds(50, 50, 110, 25);
			eventName.setFont(new Font("Serif",Font.BOLD,15));
			 backGroundPanel.add(eventName);
			
			 JLabel pakage = new JLabel("Pakage");   // Pakage Label
			 pakage.setBounds(150, 50, 110, 25);
			 pakage.setFont(new Font("Serif",Font.BOLD,15));
			  backGroundPanel.add(pakage);
			 
			 //*************** JCombo Box (Pakage list) ***************
			  
			  FixedPackageList = callDB.FixedPackageList(eventLabel);
			 
			 pakageList = new JComboBox(FixedPackageList);
			 pakageList.setBounds(50, 90, 150, 25);
			 backGroundPanel.add(pakageList);
			 pakageList.setSelectedItem(null);
			 
	 //*********************** JButton (OK Button)*************
			 
//			 Action selectPakageAction = new AbstractAction("OK") {
//
//					public void actionPerformed(ActionEvent arg0) {
//					
//						new fixedPakageDate(selectedEvent.this);												
//					}
//				};
			 
			 selectPakage= new JButton("OK");
			 selectPakage.setBounds(220, 90, 60, 25);
			 selectPakage.addActionListener(this);
			 backGroundPanel.add(  selectPakage);
			 
			 //*********************JLabel (Create  Pakage)*************
			 
			 JLabel fixedPakage = new JLabel("Create Fixed Pakage");   //Create Fixed Pakage Label
			 fixedPakage.setBounds(50, 150, 200, 25);
			 fixedPakage.setFont(new Font("Serif",Font.BOLD,15));
			  backGroundPanel.add(fixedPakage);
			 
			 JLabel customizePakage = new JLabel("Create Customize Pakage");   //Create Customize Pakage Label
			 customizePakage.setBounds(50, 230, 200, 25);
			 customizePakage.setFont(new Font("Serif",Font.BOLD,15));
 			  backGroundPanel.add(customizePakage);
			 
			 //*********************** JButton (Create Fixed Pakage)*************
			 
//			 Action createFixedPakageAction = new AbstractAction("Create") {
//
//					public void actionPerformed(ActionEvent arg0) {
//					
//						new createFixedPakage(selectedEvent.this);												
//					}
//				};
//			 
			 createFixedPakage = new JButton("Create");
			 createFixedPakage.setBounds(50, 180, 80, 25);
			 createFixedPakage.addActionListener(this);
			  backGroundPanel.add( createFixedPakage);
			 
	 //*********************** JButton (Create Customize Pakage)*************
			 
//			 Action createCustomizePakageAction = new AbstractAction("Create") {
//
//					public void actionPerformed(ActionEvent arg0) {
//					
//						new customizePakageDate(selectedEvent.this);												
//					}
//				};
			 
			 createCustomizePakage = new JButton("Create");
			 createCustomizePakage.setBounds(50, 260, 80, 25);
			 createCustomizePakage.addActionListener(this);
			 backGroundPanel.add( createCustomizePakage);
			 
			 
			 
 //*********************JLabel (Checking  Booking Info)*************
			 
			 JLabel checkingBooking = new JLabel("Checking booking info");   //Checking booking info Label
			 checkingBooking.setBounds(50, 310, 200, 25);
			 checkingBooking.setFont(new Font("Serif",Font.BOLD,15));
			  backGroundPanel.add(checkingBooking);
			 
			 
 //*********************** JButton (checking booking info)*************
			 
//			 Action checkBookingInfoAction = new AbstractAction("Check") {
//
//					public void actionPerformed(ActionEvent arg0) {
//					
//						new bookingNumber(selectedEvent.this);												
//					}
//				};
			 
			 checkBookingInfo = new JButton("Check");
			 checkBookingInfo.setBounds(50, 350, 80, 25);
			 checkBookingInfo.addActionListener(this);
			  backGroundPanel.add( checkBookingInfo);	
			 
   
        // *************************** JButton (Go back)************
				
			  Action goBackAction = new AbstractAction("Go back") {

					public void actionPerformed(ActionEvent arg0) {
					
						selectedEvent.this.dispose();
						selectedEvent.showWindow();
					}
				};
			 
			 goBack= new JButton(goBackAction);
			 goBack.setBounds(430, 355, 100,25);
			 goBack.setFont(new Font("Serif",Font.BOLD,14));
			  backGroundPanel.add(goBack);
		 
			 
		    
		   
		   
			 // *********** JFrame option ************
		    setUndecorated(false);  
		    setLocationRelativeTo(null);
		    setResizable(false);
		  //  setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);	
		    setVisible(true);

	}
	
	
	
	public void hideWindow() {                  // hide window 
		this.setVisible(false);
	}
	
	public  void showWindow() {					// show window
		this.setVisible(true);
	}
	
	
	public String eventLabel ()
	{
		eventLabel =  eventName.getText();
		return eventLabel;
		
	}
	
	
	 public String selectEventPakage()
	 {
	    selectedPackage = (pakageList.getSelectedItem().toString() );
		
		return  selectedPackage;
	 }
	
//	public String newFixedPackageName(String nFPN)
//	{
//		newFixedPAckage = nFPN;
//		System.out.println("GetName" + newFixedPAckage);
//		
//		return newFixedPAckage ;
//	}
//	

	public static void main(String[] args) {
	
	//	new selectedEvent();

	}



	@Override
	public void actionPerformed(ActionEvent e) {
		
		
		if(e.getSource() == selectPakage)
		{	
			
			 if (pakageList.getSelectedItem() == null)
				{
					
					JOptionPane.showMessageDialog (null, "Select a name from the list", "Error Message", JOptionPane.ERROR_MESSAGE);    
				}
		
			 else 
			 {
				 
				 new fixedPakageDate(selectedEvent.this);
				 
			 }
			
			
		}
		
		else if (e.getSource() == createFixedPakage)
		{
				
			new createFixedPakage(selectedEvent.this,pass);
			
		}
		
		else if (e.getSource() == createCustomizePakage)
		{
			new customizePakageDate(selectedEvent.this);
			
		}
		
		else if (e.getSource() == checkBookingInfo)
		{
			new eventInfoType(selectedEvent.this);
			
		}
		
	
		else if(e.getSource() == logOutMenu)
		{
			
			
		}
			
		
		
	}

}
