package SelectedEvent;

import java.awt.Color;
import java.awt.Dialog.ModalityType;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.util.Vector;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import EventPakageCreate.fixedPakageDate;
import Login_win.BackGround;
import Login_win.LoginWindow;
import Login_win.Login_window_db;
import PostLogin_win.AfterLogin;
import pakageBookingForm.fixedPakageBooking;

public class fixedPakageDetail extends JDialog{
	
	
	
//JPanel backGroundPanel = new JPanel();
	
	Vector<String> packageinformation = new Vector<String>();
	 Vector<String> info = new Vector<String>();
	 Login_window_db callDB;
	JLabel eventName,packageName,categoryTitleName,priceValue ;
	
	public fixedPakageDetail(fixedPakageDate packageDetail)
	{
		
		super(packageDetail, ModalityType.APPLICATION_MODAL);
		setTitle("Package Detail");
		setSize(400, 500);
		
		  //***************** JPanel option *****************
		
		   ImageIcon backGroundImage = new ImageIcon(LoginWindow.class.getResource("/background/8.jpg"));
			BackGround backGroundPanel = new BackGround(backGroundImage); // adding background
		    backGroundPanel.setLayout(null);
	        add(backGroundPanel);
				// customizePakageDatePanel.setLayout(null);
				// customizePakageDatePanel.setBackground(Color.CYAN);
				//add( customizePakageDatePanel);
	        
	        
	        
	      //*************************EventName & PAckageName ***********************
			   
//			System.out.println(information.elementAt(0));
//			System.out.println(information.elementAt(1));
	        
	        
			//  info.add(information.elementAt(0));
			//  info.add(information.elementAt(1));

	        //***************************** Call Database For Information************************
	        
	        
	         // callDB.fixedPackageDetail();
		    //  packageinformation  =   callDB.fixedPackageInformation(info);

	        //****************************  Fixed package information Label ***********************
	        
	        
	                             // ********* Event name ************//
	        
	        JLabel eventNameLabel = new JLabel("Event name :");   //  Event Name Label
	        eventNameLabel.setBounds(50, 80, 110, 25);
	        eventNameLabel.setFont(new Font("Serif",Font.BOLD,15));
			backGroundPanel.add(eventNameLabel);
			
			
	        eventName = new JLabel();   //  Event Name in String 
	        eventName.setBounds(180, 80, 110, 25);
	        eventName.setFont(new Font("Serif",Font.BOLD,15));
			backGroundPanel.add(eventName);
			
			
			              //************** Package Name Label **************//
			
			
			    JLabel packageNameLabel = new JLabel("Package name :");   //  Event Name Label
		        packageNameLabel.setBounds(50, 120, 110, 25);
		        packageNameLabel.setFont(new Font("Serif",Font.BOLD,15));
				backGroundPanel.add(packageNameLabel);
				
				
		        packageName = new JLabel();   // Package Name in String 
		        packageName.setBounds(180, 120, 110, 25);
		        packageName.setFont(new Font("Serif",Font.BOLD,15));
				backGroundPanel.add( packageName);
			
			        // **************  (Category Title) *******************//
			  
			  
			  
			  JLabel categoryName = new JLabel("Category title :");   //Create Fixed Package category title Label
			  categoryName.setBounds(50, 160, 110, 25);
			  categoryName.setFont(new Font("Serif",Font.BOLD,15));
			  backGroundPanel.add(categoryName);
			  
			  
		      categoryTitleName = new JLabel();   // Fixed Package category title Name
			  categoryTitleName.setBounds(180, 160, 110, 25);
			  categoryTitleName.setFont(new Font("Serif",Font.BOLD,15));
			  backGroundPanel.add(categoryTitleName);
			  
			  
	
			               //********************(Price)**************//
				
				JLabel price = new JLabel("Total charge :");   // Price Label
				price.setBounds(50, 200, 110, 25);
				price.setFont(new Font("Serif",Font.BOLD,15));
				backGroundPanel.add(price);
				
		        priceValue = new JLabel();   // Price Value
				priceValue.setBounds(180, 200, 110, 25);
				priceValue.setFont(new Font("Serif",Font.BOLD,15));
				backGroundPanel.add(priceValue);
				
			      // *************************** JButton (Go back)************
				
				  Action goBackAction = new AbstractAction("Go back") {

						public void actionPerformed(ActionEvent arg0) {
						
							fixedPakageDetail.this.dispose();
							packageDetail.showWindow();
							
						}
					};
				 
				 JButton goBack= new JButton(goBackAction);
				 goBack.setBounds(270, 420, 100,25);
				 goBack.setFont(new Font("Serif",Font.BOLD,14));
				 backGroundPanel.add(goBack);				
							  
				  
		 // *********** JFrame option ************
	    setUndecorated(false);  
	    setLocationRelativeTo(null);
	    setResizable(false);
	    setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);	
	    setVisible(true);
	    
	}    
	

	public static void main(String[] args) {

     //   new fixedPakageDetail();

	}

}
