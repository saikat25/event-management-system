package SelectedEvent;

import java.awt.Font;
import java.awt.Dialog.ModalityType;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import Login_win.BackGround;
import Login_win.LoginWindow;
import Login_win.Login_window_db;

public class DeleteCustomizeBooking extends JDialog implements ActionListener {

	Login_window_db callDB= new Login_window_db();
	Vector<String> addBookingnumbertoList = new Vector<String>();
	JComboBox<String> bookingNumberList;
	JButton delete;
	checkCustomizeBookingInfo backTo;
	
	
	public DeleteCustomizeBooking(checkCustomizeBookingInfo checkCustomizeBookingInfo)
	{
		
		super(checkCustomizeBookingInfo, ModalityType.APPLICATION_MODAL);
		setTitle ("Booking Number");
		setSize(500, 400);
		backTo = checkCustomizeBookingInfo;
	//	bookNumber.hideWindow();
		
		
		  //***************** JPanel option *****************
		
		   ImageIcon backGroundImage = new ImageIcon(LoginWindow.class.getResource("/background/8.jpg"));
			BackGround backGroundPanel = new BackGround(backGroundImage); // adding background
		    backGroundPanel.setLayout(null);
	        add(backGroundPanel);
		
	        
	        
			JLabel bookingNumber = new JLabel("Select booking number");
			bookingNumber.setBounds(160, 40, 200, 20);
			bookingNumber.setFont(new Font("Serif",Font.BOLD,20));
			backGroundPanel.add(bookingNumber);
			
			//********************* Number List *****************
			
			addBookingnumbertoList = callDB.bookingNumberList();		
			
		    bookingNumberList = new JComboBox<String>(addBookingnumbertoList);
			bookingNumberList.setBounds(180, 100, 150, 25);
			backGroundPanel.add(bookingNumberList);
			bookingNumberList.setSelectedItem(null);
			
			//************************** Jbutton(Next)***************
			 
//			  Action nextAction = new AbstractAction("Next") {
	//
//					public void actionPerformed(ActionEvent arg0) {
//					
//						new checkBookingInfo(bookingNumber.this,bookNumber);
//						bookingNumber.this.dispose();
//					}
//				};


				 delete = new JButton("Delete");
				 delete.setBounds(180, 270, 80, 25);
				 delete.setFont(new Font("Serif",Font.BOLD,15));
				 delete.addActionListener(this);
				 backGroundPanel.add( delete);
			
			

		
			
			 // *********** JFrame option ************
		    setUndecorated(false);  
		    setLocationRelativeTo(null);
		    setResizable(false);
		 //   setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);	
		    setVisible(true);
		
		
	
		
	}
	
	
	public void hideWindow() {                  // hide window 
		this.setVisible(false);
	}
	
	public  void showWindow() {					// show window
		this.setVisible(true);
	}
	
	
	public static void main(String[] args) {
		//new DeleteCustomizeBooking();

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == delete)
		{
			if (bookingNumberList.getSelectedItem() == null)
			{
				
				JOptionPane.showMessageDialog (null, "Select a name from the list", "Error Message", JOptionPane.ERROR_MESSAGE);    
			}

			else
			{
				this.dispose();
				String deleteEvent =  bookingNumberList.getSelectedItem().toString();
				
				System.out.println(deleteEvent);
				
				int action =JOptionPane.showConfirmDialog(null, "Do you want to delete the Booking? ", "Delete Event Name", JOptionPane.YES_NO_OPTION);
				if(action != 0)
				backTo.showWindow();	
				
				callDB.deleteBookingNumber(deleteEvent,action);
				bookingNumberList.removeItem(deleteEvent);
				
			 }
			
		   }
			
		}

		
	}


