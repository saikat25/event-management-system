package SelectedEvent;

import java.awt.Font;
import java.awt.Dialog.ModalityType;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;

import Login_win.BackGround;
import Login_win.LoginWindow;
import Login_win.Login_window_db;
import NonfixedEvent.NonfixedSelectedEvent;
import NonfixedEventOption.SelectFormNameToFill;
import NonfixedEventOption.SelectedEventOption;

public class eventInfoType extends JDialog implements ActionListener{

	JLabel selectFormNameLabel ;
	JComboBox<String> formNameList ;
	Vector<String> formNameFromBD = new Vector<String>();
	JButton formName,goBack ;
	Login_window_db callDB = new Login_window_db();
	selectedEvent selectedEventPass;


	public eventInfoType(selectedEvent typeOfInfo) {
		super(typeOfInfo,ModalityType.MODELESS) ;
		setTitle("Selected Type of Info");
		setSize(400, 500);
		typeOfInfo.hideWindow();
		selectedEventPass = typeOfInfo;
		
	    
		
		  //***************** JPanel option *****************
		
	    ImageIcon backGroundImage = new ImageIcon(LoginWindow.class.getResource("/background/8.jpg"));
		BackGround backGroundPanel = new BackGround(backGroundImage); // adding background
	    backGroundPanel.setLayout(null);
      add(backGroundPanel);
		
		
      // ************** JLabel (Select Form name) *******************
      selectFormNameLabel = new JLabel("Select Info Type");   //Create NEw Form Label
      selectFormNameLabel.setBounds(150, 30, 200, 25);
      selectFormNameLabel.setFont(new Font("Serif",Font.BOLD,20));
	  backGroundPanel.add(selectFormNameLabel);
		
		
		//***************************** Select Form Name From The List*****************
		  
		 formNameFromBD.add("Fixed Package");
		 formNameFromBD.add("Customize Package");
		  
		  formNameList = new JComboBox(formNameFromBD);
		  formNameList.setBounds(150, 77, 160, 25);
		  backGroundPanel.add( formNameList) ;
		  formNameList.setSelectedItem(null);
		  formNameList.addActionListener(this);
		  
		  
			//******************************** Button (Sub Actegory)***************************
		  
		   formName = new JButton("Next");
		   formName.setBounds(200, 420, 60, 27);
		   formName.addActionListener(this);
		   backGroundPanel.add( formName);
		   
		   
			//******************************** Button (Go back)***************************
			  
		   Action goBackAction = new AbstractAction("Go back") {

			public void actionPerformed(ActionEvent e) {
					
				eventInfoType.this.dispose();
				typeOfInfo.showWindow();
			}
		};
		  goBack = new JButton(goBackAction);
		  goBack.setBounds(300, 420, 80, 27);
		  goBack.addActionListener(this);
		   backGroundPanel.add( goBack);
		
		
		
		
		
		
		
		 // *********** JFrame option ************
	    setUndecorated(false);  
	    setLocationRelativeTo(null);
	    setResizable(false);
	   // setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);	
	    setVisible(true);
		
	}


	public void hideWindow() {                  // hide window 
		this.setVisible(false);
	}
	
	public void showWindow() {					// show window
		this.setVisible(true);
	}
	
		

	
	public static void main(String[] args) {
		//new eventInfoType();

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		
		 if (e.getSource() == formName)
		 {
			 
			  if( formNameList.getSelectedItem() == "Fixed Package")
					 
					 new bookingNumber(eventInfoType.this);
		             else 
					 {
		            	 new CustomizeBookingNumber(eventInfoType.this);
					 }
		 }
		
	}

}
