package SelectedEvent;

import java.awt.Font;
import java.awt.Dialog.ModalityType;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import Login_win.BackGround;
import Login_win.LoginWindow;
import Login_win.SignUp;
import NonfixedEvent.NonfixedSelectedEvent;
import PostLogin_win.AfterLogin;

public class SelectEventType extends JDialog implements ActionListener {
	
	
	
	
	 JButton login,logout;
	 public JTextField userField;
	 public JPasswordField passFeild;
	 JLabel eventType;
	 JComboBox  selectEventTypeList;
	
	
	public SelectEventType(LoginWindow loginWindows) {
		
		
		super(loginWindows, ModalityType.APPLICATION_MODAL);
		setTitle("Selected Event Type");
		setSize(450, 350);
		loginWindows.hideWindow();
	    
	    
		  //***************** JPanel option *****************
		
		   ImageIcon backGroundImage = new ImageIcon(LoginWindow.class.getResource("/background/8.jpg"));
			BackGround backGroundPanel = new BackGround(backGroundImage); // adding background
		    backGroundPanel.setLayout(null);
	        add(backGroundPanel);
	        
	    // *********************** Select Label*********************
	        
	        eventType = new JLabel("Select Event Type");   // Event Name Label
	        eventType.setBounds(50, 40, 160, 25);
	        eventType.setFont(new Font("Serif",Font.BOLD,17));
			 backGroundPanel.add(eventType);
	
	    //************** Event Type Option *************
	        Vector<String> eventTypeList = new Vector<String>();
	        eventTypeList.add("Fixed Event");
	        eventTypeList.add("Non-fixed Event");
			 
	        selectEventTypeList = new JComboBox(eventTypeList);
	        selectEventTypeList.setBounds(50, 90, 150, 27);
			 backGroundPanel.add(selectEventTypeList);
			 selectEventTypeList.setSelectedItem(null);   

	    
	    //************** JButton (Select)****************
	    

//	    Action signInAction = new AbstractAction("Select") {            // Button action
//			public void actionPerformed(ActionEvent arg0) {
//				
//			
//				new AfterLogin(LoginWindow.this);
//				
//		
//			}
//		};
//	    
	    
	    login = new JButton("Select");
	    login.setBounds(230, 90, 80, 27);
	    login.addActionListener(this);
	    backGroundPanel.add(login);
	    
	    
	    
	    
		   //************* Button (Log out)**********
		   
//		   Action logoutAction = new AbstractAction("Log out") {
	//
//			public void actionPerformed(ActionEvent arg0) {
//			
//				afterWindow.userField.setText(null);
//				afterWindow.passFeild.setText(null);
//				AfterLogin.this.dispose();
//				afterWindow.showWindow();
//				
//				
//			}
//		};
		   
		   logout = new JButton("Log out");
		   logout.setBounds(350, 45, 80, 27);
		   logout.addActionListener(this);
		   backGroundPanel.add(logout);
	    

	    
	 
	    
	    

	    
	   // *********** JFrame option ************
	    setUndecorated(false);  
	    setLocationRelativeTo(null);
	    setResizable(false);
	//    setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);	
	    setVisible(true);
		
	
	}
	
	
	public void hideWindow() {
		
		this.setVisible(false);
		
	}
	
	public void showWindow() {
		
		this.setVisible(true);
		
	}

		

	public static void main(String[] args) {
		
	//	new SelectEventType();

	}


	@Override
	public void actionPerformed(ActionEvent e) {
		
		if(e.getSource() == login)
		{
			 if (selectEventTypeList.getSelectedItem() == null)
				{
					
					JOptionPane.showMessageDialog (null, "Select a type from the list", "Error Message", JOptionPane.ERROR_MESSAGE);    
				}
			
			 else if(selectEventTypeList.getSelectedItem().toString()=="Fixed Event")
			{
				new AfterLogin(this);
				
			}
			
			else if(selectEventTypeList.getSelectedItem().toString()=="Non-fixed Event")
			{
				
				new NonfixedSelectedEvent(this);
				
			}
				
			
		}
		
		else if (e.getSource() == logout)
		{
			LoginWindow backLoginWindow = new LoginWindow() ;
			SelectEventType.this.dispose();
			backLoginWindow.showWindow();

		}
		
		
	}

}
