package SelectedEvent;

import java.awt.Color;
import java.awt.Dialog.ModalityType;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.Vector;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import CreateNewEvent.addCategoryItem;
import CreateNewEvent.addCategoryTitle;
import CreateNewEvent.selectEvent_showinfo;
import CreateNewEvent.showEventInfo;
import Login_win.BackGround;
import Login_win.LoginWindow;
import Login_win.Login_window_db;
import PostLogin_win.AfterLogin;

public class createNewEvent extends JDialog implements ActionListener{
	
	//JPanel backGroundPanel = new JPanel();
	
	

	AfterLogin afterLogin;
//	addCategoryTitle addEvent = new addCategoryTitle();
	Login_window_db callDB = new Login_window_db();
	JTextField eventField, categoryField, categoryItemField;
	JButton create, addCategoryTitle;
	JLabel createComplete ;


	public createNewEvent(AfterLogin afterLogin,LoginWindow afteWindow)
	{
		super(afterLogin, ModalityType.APPLICATION_MODAL);
		setTitle("Create New CategoryItem");
		
		this.afterLogin = afterLogin;    // parameter class  object
	    this.afterLogin.hideWindow();
		setSize(600, 500);
		
		  //***************** JPanel option *****************
		
		   ImageIcon backGroundImage = new ImageIcon(LoginWindow.class.getResource("/background/8.jpg"));
			BackGround backGroundPanel = new BackGround(backGroundImage); // adding background
		    backGroundPanel.setLayout(null);
	        add(backGroundPanel);
	//	createPanel.setLayout(null);
	//	createPanel.setBackground(Color.CYAN);
	//add(createPanel);
		
		//************ JLable option **************
		
		JLabel eventName = new JLabel("Event name :");   // Event Name Label
		eventName.setBounds(50, 50, 110, 25);
		eventName.setFont(new Font("Serif",Font.BOLD,15));
	  
		
		backGroundPanel.add(eventName);
		
	    eventField = new JTextField(); //  Event Name TextFeild
		eventField.setBounds(170, 50, 140, 25);
		backGroundPanel.add(eventField);
		
		JLabel categoryName = new JLabel("Category title :");   // Category type Label
	    categoryName.setBounds(50, 100, 110, 25);
	    categoryName.setFont(new Font("Serif",Font.BOLD,15));
		backGroundPanel.add(categoryName);
		
		categoryField = new JTextField(); //  Category type TextFeild
		categoryField.setBounds(170, 100, 140, 25);
		backGroundPanel.add(categoryField);
		
		JLabel categoryItem = new JLabel("Category Item :");   // Category Item Label
		categoryItem.setBounds(50, 150, 110, 25);
		categoryItem.setFont(new Font("Serif",Font.BOLD,15));
		backGroundPanel.add(categoryItem);
		
		categoryItemField = new JTextField(); //  Category Item TextFeild
		categoryItemField .setBounds(170, 150, 140, 25);
		backGroundPanel.add( categoryItemField );
		
		//***************************** Create Complete label****************
		
		   createComplete = new JLabel();
		   createComplete.setFont(new Font("Serif", Font.BOLD, 14));
		   createComplete.setBounds(170, 200, 100, 30);
		   backGroundPanel.add(createComplete);
		
		
		//********************* Add Category Title ****************
		
//		   Action addCategoryTitleAction = new AbstractAction("Add category title") {
//
//					public void actionPerformed(ActionEvent arg0) {
//					
//							new addCategoryTitle(createNewEvent.this);												
//					}
//				};
//		
		
		addCategoryTitle = new JButton("Add category title");
		addCategoryTitle.setBounds(420, 100, 140, 25);
		addCategoryTitle.addActionListener(this);
		backGroundPanel.add(addCategoryTitle);
		
		
		// ***********************Add Category Item *****************
		
		   Action addCategoryItemAction = new AbstractAction("Add category Item") {

				public void actionPerformed(ActionEvent arg0) {
				
				new addCategoryItem(createNewEvent.this);	
					//addCategoryTitle addCT = new addCategoryTitle(createNewEvent.this);
				}
			};
	
	
	JButton addCategoryItem = new JButton(addCategoryItemAction);
	addCategoryItem.setBounds(420, 150, 140, 25);
	backGroundPanel.add(addCategoryItem);
	
	//**********************  Show event info Button *****************
	
	   Action showInfoAction = new AbstractAction("Show event info") {

			public void actionPerformed(ActionEvent arg0) {
			
				new selectEvent_showinfo(createNewEvent.this);											
			}
		};


		JButton showInfo = new JButton(showInfoAction);
		showInfo.setBounds(420, 200, 140, 25);
		backGroundPanel.add(showInfo);
	
	
	//**********************  Button Create*****************
		
	
//	   Action createAction = new AbstractAction("Create") {
//
//			public void actionPerformed(ActionEvent arg0) {
//			
//				System.out.println("Event Created");								
//			}
//		};


         create = new JButton("Create");
         create.setBounds(205, 350, 100, 25);
         create.addActionListener(this);
         backGroundPanel.add( create);
	
	
	//********************** Go back ********************
	
	   Action goBackAction = new AbstractAction("Go back") {

				public void actionPerformed(ActionEvent arg0) {
				
				createNewEvent.this.dispose();	
				createComplete.setText(null);// current window close
				afterLogin.showWindow();
					
				//	new AfterLogin();												
				}
			};
	
	
	JButton goBack = new JButton(goBackAction);
	 goBack.setBounds(460, 350, 100, 25);
	backGroundPanel.add( goBack);
	
		
		
		//************************* Log out ***********************
		
		   Action logoutAction = new AbstractAction("Log out") {

				public void actionPerformed(ActionEvent arg0) {
				
					createNewEvent.this.dispose();  // current window close
					createComplete.setText(null);
					afteWindow.showWindow();        // Back to Login window
					
					
				}
			};
			   
			   JButton logout = new JButton(logoutAction);
			   logout.setBounds(480, 50, 80, 27);
			  backGroundPanel.add(logout);
			  
			  

		
		 // *********** JFrame option ************
	    setUndecorated(false);  
	    setLocationRelativeTo(null);
	    setResizable(false);
	  //  setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);	
	    setVisible(true);
	}
	
	
	public void hideWindow() {                  // hide window 
		this.setVisible(false);
	}
	
	public void showWindow() {					// show window
		this.setVisible(true);
	}


	public static void main(String[] args) {
		//new createNewEvent();

	}


	@Override
	public void actionPerformed(ActionEvent createNewEvent) {
		
		Vector<String> newEventInput = new Vector<String>();
		Vector<String> eventSelect = new Vector<String>();
		Vector<String> eventList = new Vector<String>();
		boolean check = false;
	
		boolean returnValue;
		
		newEventInput.add(eventField.getText());
		newEventInput.add(categoryField.getText());
		newEventInput.add(categoryItemField.getText());

		if(createNewEvent.getSource() == this.create)
		{
		
		 createComplete.setText(null);	
		 returnValue= callDB.createNewEventInput(newEventInput);
		// addEvent.addEventList();
		 createComplete.setText("Event created");
		 
	
		 eventField.setText (null);
		 categoryField.setText(null); //review  
		 categoryItemField.setText (null);
		 createComplete.setText(" ");
		 
		}
		
		else if(createNewEvent.getSource() == addCategoryTitle)
		{
			
			new addCategoryTitle(createNewEvent.this);
		//	eventList = callDB.addEventToSelectList();
			//System.out.println("EventList" + eventList);
			
			
		}
		
	}
}
