package SelectedEvent;

import java.awt.Color;
import java.awt.Dialog.ModalityType;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import CreateNewEvent.selectEvent_showinfo;
import Login_win.BackGround;
import Login_win.LoginWindow;
import Login_win.Login_window_db;
import PostLogin_win.AfterLogin;

public class bookingNumber extends JDialog implements ActionListener {
	
//JPanel backGroundPanel = new JPanel();
	
	Login_window_db callDB= new Login_window_db();
	Vector<String> addBookingnumbertoList = new Vector<String>();
	JComboBox<String> bookingNumberList;
	JButton next;
	eventInfoType bN;
	int entryNum;
	
	public bookingNumber(eventInfoType bookNumber)
	
	{
		
		super(bookNumber, ModalityType.APPLICATION_MODAL);
		setTitle ("Booking Number");
		setSize(500, 400);
		bookNumber.hideWindow();
		bN = bookNumber;
		
		  //***************** JPanel option *****************
		
		   ImageIcon backGroundImage = new ImageIcon(LoginWindow.class.getResource("/background/8.jpg"));
			BackGround backGroundPanel = new BackGround(backGroundImage); // adding background
		    backGroundPanel.setLayout(null);
	        add(backGroundPanel);
		
		//backGroundPanel.setLayout(null);
		//backGroundPanel.setBackground(Color.CYAN);
		//add(backGroundPanel);
		
		//******************* JLabel option ****************
	        
	        
		
		JLabel bookingNumber = new JLabel("Select booking number");
		bookingNumber.setBounds(160, 40, 200, 20);
		bookingNumber.setFont(new Font("Serif",Font.BOLD,20));
		backGroundPanel.add(bookingNumber);
		
		//********************* Number List *****************
		
		addBookingnumbertoList = callDB.fixedbookingNumberList();		
		
	    bookingNumberList = new JComboBox<String>(addBookingnumbertoList);
		bookingNumberList.setBounds(180, 100, 150, 25);
		backGroundPanel.add(bookingNumberList);
		bookingNumberList.setSelectedItem(null);
		
		//************************** Jbutton(Next)***************
		 
//		  Action nextAction = new AbstractAction("Next") {
//
//				public void actionPerformed(ActionEvent arg0) {
//				
//					new checkBookingInfo(bookingNumber.this,bookNumber);
//					bookingNumber.this.dispose();
//				}
//			};


			 next = new JButton("Next");
			 next.setBounds(180, 270, 80, 25);
			 next.setFont(new Font("Serif",Font.BOLD,15));
			 next.addActionListener(this);
			 backGroundPanel.add( next);
		
		
		 
				// *************************** JButton (Go back)************
				
			  Action goBackAction = new AbstractAction("Go back") {

					public void actionPerformed(ActionEvent arg0) {
					
						bookingNumber.this.dispose();
						bookNumber.showWindow();
					}
				};
			 
			 JButton goBack= new JButton(goBackAction);
			 goBack.setBounds(370, 270, 90, 25);
			 goBack.setFont(new Font("Serif",Font.BOLD,15));
			 backGroundPanel.add(goBack);		
		
		
		 // *********** JFrame option ************
	    setUndecorated(false);  
	    setLocationRelativeTo(null);
	    setResizable(false);
	 //   setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);	
	    setVisible(true);
		
		
		
	}
	
	
	
	public void hideWindow() {                  // hide window 
		this.setVisible(false);
	}
	
	public void showWindow() {					// show window
		this.setVisible(true);
	}
	
	
	public String getFormInfo()
	{	
		
		
		entryNum = Integer.parseInt( bookingNumberList.getSelectedItem().toString()) ;

		String x = " ";

	       Vector<String>receivedData = callDB.showFixedPackageBookingInfo(entryNum);
			
			for(int i=0;i<receivedData.size();i++)
			{
				x+=receivedData.elementAt(i);
				
			}
		 
			System.out.println(x);
			
			System.out.println("number booked ");

		return x;
			
	}
		
		
	
	
//	public Vector<String> showBookingInfo()
//	{
//		
//		Vector<String> receiveBookingInfo  = new Vector<String>();
//		
//		return 
//		
//	}
 
	public static void main(String[] args) {
		
	//	new bookingNumber();

	}

	@Override
	public void actionPerformed(ActionEvent e) {
	
		if(e.getSource()== next )
		{
			
			if (bookingNumberList.getSelectedItem() == null)
				{
					
					JOptionPane.showMessageDialog (null, "Select a name from the list", "Error Message", JOptionPane.ERROR_MESSAGE);    
				}
		
			 else 
			 {
				 
//					String x = " ";
//					Vector<String>receivedData = callDB.showFixedPackageBookingInfo();
//					
//					for(int i=0;i<receivedData.size();i++)
//					{
//						x+=receivedData.elementAt(i);
//						
//					}
//				 
//					System.out.println(x);
//					
//					System.out.println("number booked ");
				 
				 //	getFormInfo();
				
					new checkBookingInfo(bookingNumber.this,bN);
					bookingNumber.this.dispose();
					
				}
				 
			 }

		}
		
	}


