package SelectedEvent;

import java.awt.Font;
import java.awt.Dialog.ModalityType;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;

import Login_win.BackGround;
import Login_win.LoginWindow;
import Login_win.Login_window_db;

public class checkCustomizeBookingInfo extends JDialog implements ActionListener{

	
	
	Login_window_db callDB = new Login_window_db();
	Vector<String> receiveBookingInfo = new Vector<String>();
	JTextArea infoArea;
	JButton deleteBooking, ok;
	eventInfoType backTo;
	public checkCustomizeBookingInfo(CustomizeBookingNumber customizeBookingNumber, eventInfoType bN)
	{
		
		super(customizeBookingNumber, ModalityType.APPLICATION_MODAL);
		setTitle("Check Booking Info");
		setSize(450, 650);
		customizeBookingNumber.hideWindow();
		backTo= bN;
		
		  //***************** JPanel option *****************
		
		   ImageIcon backGroundImage = new ImageIcon(LoginWindow.class.getResource("/background/8.jpg"));
			BackGround backGroundPanel = new BackGround(backGroundImage); // adding background
		    backGroundPanel.setLayout(null);
	        add(backGroundPanel);
	
		
		
		//********************* Top Label *****************
	        
		JLabel checkbooking = new JLabel("Check booking");   // Event Name Label
		checkbooking.setBounds(150, 30, 150, 25);
		checkbooking.setFont(new Font("Serif",Font.BOLD,20));
		backGroundPanel.add(checkbooking);
		
		
		//********************************** Show Info in a Text Area **************************
		
		
	//	 receiveBookingInfo = callDB.showFixedPackageBookingInfo();
		
		// infoArea.setText("");
//			String x = "";
//			Vector<String>receivedData = callDB.showFixedPackageBookingInfo();
//			
//			for(int i=0;i<receivedData.size();i++)
//			{
//				x+=receivedData.elementAt(i);
//			}
		//	infoArea.setText(x);

		String showinfo = customizeBookingNumber.getFormInfo();
		
	     infoArea = new JTextArea(showinfo);
	   //  infoArea.setBounds(25, 70, 400 ,460);
	     infoArea.setFont(new Font("Serif",Font.BOLD,20));
	     JScrollPane scrollPane = new JScrollPane(infoArea); // Add Scroll bar to Text area
		  scrollPane.setBounds(25, 70, 400 ,460);
		  scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		  backGroundPanel.add(scrollPane);
		 
		 
		
		 

		
		//********************** JButton (OK)****************
		
//		  Action okAction = new AbstractAction("OK") {
//
//				public void actionPerformed(ActionEvent arg0) {
//				
//					checkBookingInfo.this.dispose();
//					bookNumber.showWindow();
//			      												
//				}
//			};
	
	
	ok = new JButton("OK");
	ok.setBounds(230, 550, 60, 25);
	ok.addActionListener(this);
	backGroundPanel.add(ok);
	
	
	//*************************** JButton (Delete booking)*****************
	
//	
//	  Action deleteBookingAction = new AbstractAction("Delete booking") {
//
//			public void actionPerformed(ActionEvent arg0) {
//				
//				
//			
//				JOptionPane.showConfirmDialog(null, "Do you want to delete the booking ? ", // confirm box
//						"Confirmation Window",JOptionPane.YES_NO_OPTION);											
//			}
//		};


		deleteBooking = new JButton("Delete booking");
		deleteBooking.setBounds(310, 550, 120, 25);
		deleteBooking.addActionListener(this);
		backGroundPanel.add(deleteBooking);
	
		
		
		
		 // *********** JFrame option ************
	    setUndecorated(false);  
	    setLocationRelativeTo(null);
	    setResizable(false);
	    setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);	
	    setVisible(true);

		
	}
			
	public void hideWindow() {                  // hide window 
		this.setVisible(false);
	}
	
	public  void showWindow() {					// show window
		this.setVisible(true);
	}
	
	
	public static void main(String[] args) {
		//new checkCustomizeBookingInfo();

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == ok)
		{
			
			checkCustomizeBookingInfo.this.dispose();
		    backTo.showWindow();
			
		}
		
		
	    else if(e.getSource() == deleteBooking )
		{
			
            new DeleteCustomizeBooking(checkCustomizeBookingInfo.this);
			
		}
	}

}
