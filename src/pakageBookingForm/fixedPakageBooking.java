package pakageBookingForm;

import java.awt.Color;
import java.awt.Dialog.ModalityType;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import EventPakageCreate.fixedPakageDate;
import Login_win.BackGround;
import Login_win.LoginWindow;
import Login_win.Login_window_db;
import SelectedEvent.selectedEvent;

public class fixedPakageBooking extends JDialog implements ActionListener{
	
	
//	JPanel backGroundPanel = new JPanel();
	String packageTotal, totalCharge,advanceCharge,addEventName ;
	int balanceCalulated,totalC,advanceC;
	JTextField nameField , addressField, contactField ,timeOfpartyField ,numOfpersonField,
	totalChargeField , advanceChargeField ,balanceAmountField;
	Login_window_db callDB = new Login_window_db();
	
	JButton balanceAmount,Register;
	selectedEvent eventName;
	fixedPakageDate packageBookPass;
	
	
	public fixedPakageBooking(fixedPakageDate packageBook , selectedEvent packageDate)
	
	{
		
		super(packageBook, ModalityType.APPLICATION_MODAL);
		setTitle ("Fixed Pakage Booking");
		setSize(480, 650);
		packageBook.hideWindow();
		eventName = packageDate ;
		packageBookPass = packageBook;
		  //***************** JPanel option *****************
		
		 ImageIcon backGroundImage = new ImageIcon(LoginWindow.class.getResource("/background/8.jpg"));
			BackGround backGroundPanel = new BackGround(backGroundImage); // adding background
		    backGroundPanel.setLayout(null);
	        add(backGroundPanel);
		
	//	customizePakageBookingPanel.setLayout(null);
	//	customizePakageBookingPanel.setBackground(Color.CYAN);
	//	add(customizePakageBookingPanel);
		
		
		//**************** JLabel (Booking Label)****************
		
		JLabel bookingLabel = new JLabel("Booking form");   // Event Name Label
		bookingLabel.setBounds(180, 30, 130, 25);
		bookingLabel.setFont(new Font("Serif",Font.BOLD,18));
		backGroundPanel.add(bookingLabel);
		
	
		JLabel Name = new JLabel("Name :");   // Event Name Label
		Name.setBounds(50, 80, 110, 25);
		Name.setFont(new Font("Serif",Font.BOLD,15));
		backGroundPanel.add(Name);
		
		nameField = new JTextField(); //  Event Name TextFeild
		nameField.setBounds(190, 80, 160, 25);
		nameField.addActionListener(this);
		backGroundPanel.add(nameField);
		
		JLabel address = new JLabel("Address :");   // Event address Label
		address.setBounds(50, 130, 110, 25);
		address.setFont(new Font("Serif",Font.BOLD,15));
		backGroundPanel.add(address);
		
		addressField = new JTextField(); //  Event address TextFeild
		addressField.setBounds(190, 130, 160, 25);
		addressField.addActionListener(this);
		backGroundPanel.add(addressField);
		
		JLabel Contact = new JLabel("Contact No. :");   // Event contact Label
		Contact.setBounds(50, 180, 140, 25);
		Contact.setFont(new Font("Serif",Font.BOLD,15));
		backGroundPanel.add(Contact);
		
		contactField = new JTextField(); //  Event contact TextFeild
		contactField.setBounds(190, 180, 160, 25);
		contactField.addActionListener(this);
		backGroundPanel.add(contactField);
		
		
		JLabel timeOfparty = new JLabel("Time of Party :");   // Event time of party Label
		timeOfparty.setBounds(50, 230, 140, 25);
		timeOfparty.setFont(new Font("Serif",Font.BOLD,15));
		backGroundPanel.add(timeOfparty);
		
		timeOfpartyField = new JTextField(); //  Event time of party  TextFeild
		timeOfpartyField.setBounds(190, 230, 160, 25);
		timeOfpartyField.addActionListener(this);
		backGroundPanel.add(timeOfpartyField);
		
		JLabel numOfperson = new JLabel("Num. of Person :");   // Event number of person   Label
		numOfperson.setBounds(50, 280, 140, 25);
		numOfperson.setFont(new Font("Serif",Font.BOLD,15));
		backGroundPanel.add(numOfperson);
		
		numOfpersonField = new JTextField(); //  Event number of person   TextFeild
		numOfpersonField.setBounds(190, 280, 160, 25);
		numOfpersonField.addActionListener(this);
		backGroundPanel.add(numOfpersonField);
		
		JLabel totalCharge = new JLabel("Total charge :");   // Event Total charge    Label
		totalCharge.setBounds(50, 330, 140, 25);
		totalCharge.setFont(new Font("Serif",Font.BOLD,15));
		backGroundPanel.add(totalCharge);
		
		totalChargeField = new JTextField(); //  Event Total charge  TextFeild
		totalChargeField.setBounds(190, 330, 160, 25);
		totalChargeField.addActionListener(this);
		backGroundPanel.add(totalChargeField);
		
		
		JLabel advanceCharge = new JLabel("Advance charge :");   // Event Advance charge    Label
		advanceCharge.setBounds(50, 380, 140, 25);
		advanceCharge.setFont(new Font("Serif",Font.BOLD,15));
		backGroundPanel.add(advanceCharge);
		
		advanceChargeField = new JTextField(); //  Event Advance charge  TextFeild
		advanceChargeField.setBounds(190, 380, 160, 25);
		advanceChargeField.addActionListener(this);
		backGroundPanel.add(advanceChargeField);
		
		
		balanceAmount = new JButton("Balance amount");   // Event rest of charge button
		balanceAmount.setBounds(50, 430, 140, 25);
		balanceAmount.setFont(new Font("Serif",Font.BOLD,15));
		balanceAmount.addActionListener(this);
		backGroundPanel.add(balanceAmount);
		
		balanceAmountField = new JTextField(); //  Event Advance charge  TextFeild
		balanceAmountField.setBounds(190, 430, 160, 25);
		balanceAmountField.addActionListener(this);
		backGroundPanel.add(balanceAmountField);
		
		
		  
	// *************************** JButton (Booking)************
		
//			  Action registerAction = new AbstractAction("Booking") {
//
//					public void actionPerformed(ActionEvent arg0) {
//					
//						//new fixedPakageBooking();												
//					}
//				};
			 
			 Register= new JButton("Booking");
			 Register.setBounds(190, 530, 100, 25);
			 Register.setFont(new Font("Serif",Font.BOLD,14));
			 Register.addActionListener(this);
			 backGroundPanel.add(Register);
			 
			 
			  
				// *************************** JButton (Go back)************
					
						  Action goBackAction = new AbstractAction("Go back") {

								public void actionPerformed(ActionEvent arg0) {
								
									fixedPakageBooking.this.dispose();
									packageDate.showWindow();
								}
							};
						 
						 JButton goBack= new JButton(goBackAction);
						 goBack.setBounds(360, 530, 100,25);
						 goBack.setFont(new Font("Serif",Font.BOLD,14));
						 backGroundPanel.add(goBack);
		
	
		
		// *********** JFrame option ************
	    setUndecorated(false);  
	    setLocationRelativeTo(null);
	    setResizable(false);
	    setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);	
	    setVisible(true);
		
		
		
	}
	
	
	public int calculate (String totalCharge , String advanceCharge)
	{
		 totalC = Integer.parseInt(totalCharge);
		 advanceC = Integer.parseInt(advanceCharge);
		int balance = totalC - advanceC ;

		return balance;
	}


	public static void main(String[] args) {
	
		//new fixedPakageBooking();
	}


	@Override
	public void actionPerformed(ActionEvent e) {

		Vector<String> pakageBooking = new Vector<String>();
		pakageBooking.add(nameField.getText());
		pakageBooking.add(addressField.getText());
		pakageBooking.add(contactField.getText());
		pakageBooking.add(timeOfpartyField.getText());
		pakageBooking.add(numOfpersonField.getText());
		pakageBooking.add(totalChargeField.getText());
		pakageBooking.add(advanceChargeField.getText());
		pakageBooking.add(balanceAmountField.getText());
		 String ev = eventName.eventLabel();
		System.out.println(eventName);
		String selectedDate = packageBookPass.packageDate();
		System.out.println(selectedDate);
		
		pakageBooking.add(selectedDate);
		pakageBooking.add(ev);
		
	
		
		
		if(e.getSource() ==  Register )
		{
			
			Vector<String> passInfo = new Vector<String>(); 
			
			passInfo.add(selectedDate);
			passInfo.add(ev);

			int BN = callDB.searchBookingNumber();
			if(BN==0)
			{
				BN=0;
				callDB.fixedPackageBookingFormDAta(pakageBooking,BN);
				callDB.addbookingNumber(passInfo,BN);	
				
			}
			
			else
			{
				BN++;
				callDB.fixedPackageBookingFormDAta(pakageBooking,BN);
				callDB.addbookingNumber(passInfo,BN);
				
			}
			
			nameField.setText(null)  ;
	        addressField.setText(null);
	        contactField.setText(null);
			timeOfpartyField.setText(null);
			numOfpersonField.setText(null);
			totalChargeField.setText(null);
			advanceChargeField.setText(null);
			balanceAmountField.setText(null);
			
			
		}
		else if (e.getSource() == balanceAmount)
		{
			
			totalCharge = pakageBooking.elementAt(5);
			advanceCharge = pakageBooking.elementAt(6);
		    balanceCalulated = calculate(totalCharge, advanceCharge);
		    balanceAmountField.setText(Integer.toString(balanceCalulated));
		

		}
		
	}

}
