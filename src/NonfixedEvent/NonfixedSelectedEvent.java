package NonfixedEvent;

import java.awt.Font;
import java.awt.Dialog.ModalityType;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Calendar;
import java.util.Vector;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import com.sun.xml.internal.ws.api.ha.StickyFeature;

import Login_win.BackGround;
import Login_win.LoginWindow;
import Login_win.Login_window_db;
import Login_win.SignUp;
import NonfixedEventOption.CreateNewEvent;
import NonfixedEventOption.CreateNewForm;
import NonfixedEventOption.SelectFormNameToFill;
import NonfixedEventOption.SelectTypeOfInformation;
import NonfixedEventOption.SelectedEventOption;
import NonfixedEventOption.showInfomation;
import PostLogin_win.AfterLogin;
import SelectedEvent.SelectEventType;

public class NonfixedSelectedEvent extends JDialog implements ActionListener {
	
	JComboBox<String>  eventSelecList ;
	JButton  logout,newevent, select,delete,createForm,showinformation ,formFill, goBack;
	SelectEventType nonFixedSelectedEvent ;
	String  selectedItem;
	Login_window_db callDB = new Login_window_db();
	Vector<String> eL = new Vector<String>();
	

	
	public NonfixedSelectedEvent(SelectEventType nonFixedSelectedEvent)
	{
		
		super(nonFixedSelectedEvent,ModalityType.MODELESS) ;
		setTitle("New User");
		setSize(600, 500);
		nonFixedSelectedEvent.hideWindow();
		
		
	    
	    //***************** JPanel option *****************
		
		 ImageIcon backGroundImage = new ImageIcon(LoginWindow.class.getResource("/background/8.jpg"));
			BackGround backGroundPanel = new BackGround(backGroundImage); // adding background
		    backGroundPanel.setLayout(null);
	        add(backGroundPanel);
		
		
	   // signUpPanel.setLayout(null);
	  //  signUpPanel.setBackground(Color.CYAN);
	   // add(signUpPanel);
	    
	        //********************* JLabel option **************
	 	   
	 	   JLabel eventSelection = new JLabel("Event selection");
	 	   eventSelection.setFont(new Font("Serif", Font.BOLD, 20));
	 	   eventSelection.setBounds(50, 50, 130, 30);
	 	   backGroundPanel.add(eventSelection);
	 	   
	 	   // ******************** JCombo box (event Selection List)**************
	 	   
	 	    eL = callDB.getNonFixedEventList ();              // add event to Select List
	 	   
	 	   eventSelecList = new JComboBox<String>( eL);
	 	   eventSelecList.setBounds(50, 100, 130, 25);
	 	   backGroundPanel.add(eventSelecList);
	 	   eventSelecList.setSelectedItem(null);
	 	   
	 	   //************* Button (Log out)**********
	 	   
//	 	   Action logoutAction = new AbstractAction("Log out") {
	 //
//	 		public void actionPerformed(ActionEvent arg0) {
//	 		
//	 			afterWindow.userField.setText(null);
//	 			afterWindow.passFeild.setText(null);
//	 			AfterLogin.this.dispose();
//	 			afterWindow.showWindow();
//	 			
//	 			
//	 		}
//	 	};
	 	   
	 	   logout = new JButton("Log out");
	 	   logout.setBounds(480, 65, 80, 27);
	 	   logout.addActionListener(this);
	 	   backGroundPanel.add(logout);
	 	   
	 	   // ************** Button (Create New Event)*************
	 	   
//	 	   
//	 	   Action neweventAction = new AbstractAction("Create New Event") {
	 //
//	 			public void actionPerformed(ActionEvent arg0) {
//	 			
//	 				Object addCTg;
//	 				new createNewEvent(AfterLogin.this,afterWindow);
//	 			
//	 				
//	 			}
//	 		};
	 		   
	 		   newevent = new JButton("Create New Event");
	 		   newevent.setBounds(420, 115, 140, 27);
	 		   newevent.addActionListener(this);
	 		   backGroundPanel.add(newevent);
	 		   
	 		   
	//************************************Button (Go back)************************

	 		   Action BacktoPrevious = new AbstractAction("Go back") {
		 			
		 			 			public void actionPerformed(ActionEvent e) {
		 			 					
		 			 				NonfixedSelectedEvent.this.dispose();
		 							nonFixedSelectedEvent.showWindow();
		 			 				
		 			 			}
		 			 		};		 		   
			 		goBack = new JButton(BacktoPrevious);
			 		goBack.setBounds(420, 350, 140, 27);
			 		//goBack.addActionListener(this);
				    backGroundPanel.add(goBack);		   
	 		   
	 		   
	 		   // *************************  Button (select) *************
	 		   
//	 		   Action selectAction = new AbstractAction("Select") {
	 //
//	 			public void actionPerformed(ActionEvent e) {
//	 					
//	 				new selectedEvent(AfterLogin.this);
//	 				
//	 			}
//	 		};
	 		   
	 		   
	 		   select = new JButton("Select");
	 		   select.setBounds(50, 350, 70, 27);
	 		   select.addActionListener(this);
	 		   backGroundPanel.add(select);
	 		   
	 		   
	    // *************************  Button Delete) *************
	 		   
//	 		   Action deleteAction = new AbstractAction("Delete") {
	 //
//	 			public void actionPerformed(ActionEvent e) {
//	 					
//	 				JOptionPane.showConfirmDialog(null, "Do you want to delete the booking ? ", // confirm box
//	 						"Confirmation Window",JOptionPane.YES_NO_OPTION);
//	 				
//	 			}
//	 		};
	 		   
	 		   
	 		   delete = new JButton("Delete");
	 		   delete.setBounds(140, 350, 70, 27);
	 		   delete.addActionListener(this);
	 		   backGroundPanel.add(delete);
	 		
	 		
	 		 // *********** JFrame option ************
	 	    setUndecorated(false);  
	 	    setLocationRelativeTo(null);
	 	    setResizable(false);
	 	  //setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);	// running window close 
	 	    setVisible(true);
	 	}
	
	public void hideWindow() {                  // hide window 
		this.setVisible(false);
	}
	
	public void showWindow() {					// show window
		this.setVisible(true);
	}
	
	
	 public String selectEvent()
	 {
	    selectedItem = ( eventSelecList.getSelectedItem().toString() );
		
		return selectedItem;
	 }
	
	public static void main(String[] args) {
	 //new NonfixedSelectedEvent();

	}

	@Override
	public void actionPerformed(ActionEvent e) {
	
		try {
			
			
			if(e.getSource()  == newevent)
			{
				
				new CreateNewEvent(this);
				
			}
			
	
			else if(e.getSource() == logout)
			{
				LoginWindow afterWindow = new LoginWindow();
				NonfixedSelectedEvent.this.dispose();
				afterWindow.showWindow();
				
				
			}
			
			else if (e.getSource() == select)
			{
				
				 if (eventSelecList.getSelectedItem() == null)
				{
					
					JOptionPane.showMessageDialog (null, "Select a name from the list", "Error Message", JOptionPane.ERROR_MESSAGE);    
				}
				 
				 else
				 {
					 new SelectedEventOption(this);
					 
				 }
				
				
				
				
			}
			
			else if( e.getSource()== delete)
			{
				
			//	String deleteEvent = this.selectEvent();
        		JOptionPane.showConfirmDialog(delete, "Do you want to delete the booking ? ", "Confirmation Window", JOptionPane.YES_NO_OPTION);
				
				
			}
			
		} catch (Exception nulException) {
			// TODO: handle exception
		}
	
		
		// TODO Auto-generated method stub
		
	}

}
